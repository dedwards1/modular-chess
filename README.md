# Modular Chess

This project is a reference implementation for using
hexagonal architecture and domain-driven-design techniques
to create a modular system that lends itself to
cheap-and-easy shuffling of deployed service boundaries,
based on [this talk](https://pivotal.zoom.us/rec/share/395SK-z73UpJbK_mtW3zf5QEJ9i5aaa8hCAb-PYNmkiSZIZhE9C8IgJs4kpZW4bq).


## Warning: Git Shenanigans

Be wary when cloning this repository. I'm using git branches
for not-their-intended purpose:
instead of different works-in-progress that will eventually
be merged, I have branches which showcase applications
of different techniques on the codebase.

This means that as I make improvements to this codebase,
I'll often need to manipulate those branches (rebasing,
modifying old commits, etc.) in ways that change history.

Running your usual `git pull` command could therefore
have effects you won't like. Instead, to pull updates
from this repo, run:

```bash
$ git fetch
$ git checkout whatever-branch
$ git reset --hard origin/whatever-branch
```

(where "whatever-branch" is "master", "adapter-division", etc.)

## Run it!

To start the system locally, run:

```bash
$ ./gradlew bootRun --parallel
```

Then in your browser, navigate to `http://localhost:8080`.

Create a few different players, and simulate their actions
by switching accounts with the "Sign In" buttons.
(There's no authentication in this toy example.)

You can invite someone to play by creating a game,
accept or decline an invitation, and sort of play chess!

(This isn't a complete chess engine. You can make all kinds
of illegal moves, and it can't handle "special" moves like
castles, pawn promotions, or en-passant captures.)

## Points of Interest

The codebase showcases a number of techniques described in the talk:

- The system is divided into a number of **bounded contexts**,
  each with its own `policy` module defining the context's
  domain model, and `adapter` modules that plug into the policy.
  * `gameplay` concerns the rules of chess,
    and how an individual game of chess is played.
    
  * `organizing-games` concerns creating games,
    inviting specific players to games, and accepting invitations.
  
  * `web-app` handles specific issues related to delivering
    the application to browsers. (Unlike the others, this folder doesn't
    contain a policy module; see "Breaking the Rules" below.)

- Because each bounded context has a separately-defined domain,
  they can have appropriately-specialized definitions
  of similar concepts.
  See how both `organizing-games-policy` and `gameplay-policy`
  define a `Game` model, but these models are tailored
  to their respective contexts.

- The `deployables/chess-app` module is the only "shippable" module,
  and pulls in and wires up all the other modules.

- The `organizing-games` and `gameplay` contexts interact
  when starting a game. This is accomplished by the
  `organizing-games/organizing-games-gameplay-adapter` module,
  which plugs into **both** policies.

- The `organizing-games` and `gameplay` contexts both include
  adapters for SQL databases. There's a few interesting maneuvers there:
  - There is a module in both contexts devoted to "port contracts".
    The port-contracts modules contain abstract tests defining behaviors
    which the policy module expects secondary-adapter implementations to have in order to work.
    The two sql adapter modules implement the appropriate contracts in their tests
    in order to ensure that they are valid implementations.

  - The sql adapter modules themselves do not define any database migrations.
    It can be tempting to put migrations there, since they seem like database concerns,
    but it's a lot less trouble to consider them concerns of the deployable
    (that is, each deployable module owns the migrations to put its own database
    into the state that it needs).

    The sql adapter modules *do* contain a `schema.sql` file in their test resources,
    which serves as a reference when writing migrations in the deployable module
    ("this is the target schema we need to get to").

  - I opted to implement the sql adapter for `organizing-games` using Spring JPA,
    and the sql adapter for `gameplay` using Spring JDBC,
    just to showcase what following the module-dependency rules looks like with those libraries.

    For example, people sometimes get tripped up by the fact that your JPA repository interface,
    despite being an interface, _does not_ work as a "secondary adapter" interface,
    because it is coupled to database entity definitions. Instead, you need to use the JPA interface
    from inside an adapter class you write yourself, which implements an interface
    defined in your policy module. See `SqlBackedPlayerRepository` for an example.

Other branches in this repository show how the system can be divided
into multiple deployables via various maneuvers:
- The `monolithic-deployment` branch shows the system all deployed
  in a single service

- The `deployable-mitosis` branch shows the simplest maneuver
  for extracting a separate service

- The `adapter-division` branch shows how to separate two contexts
  which are connected by a cross-context adapter. (The three steps
  of the maneuver are spread over three commits, so be sure to
  look at the commit log.)

- The `adapter-extraction` branch shows how to extract a single adapter
  into a separate service. (This branch also involves steps spread across
  several commits, the commit messages for which give more detail about each.)

## Breaking the Rules

There are always places where under the circumstances,
it makes sense to break the rules.

In this version of the application, the `web-app` bounded context
breaks the usual rule of having a policy module.
Instead of interacting with the other contexts through defined ports,
its `frontend-adapter` just proxies frontend calls
directly to the other APIs.

This avoids the pain of having to modify ports in multiple contexts
as the `organizing-games` and `gameplay` policies evolve,
 while leaving space for the `frontend-adapter`
 to do useful things like authenticate calls from browsers
and utilize service-discovery tooling.

The cost we pay for foregoing the policy definition
is that testing any logic we put into `frontend-adapter`
will be trickier, and might require special tooling
(like Wiremock).

But these are just choices--in this case, I guessed that
the cost of having to maintain additional port definitions
in the `web-app` context would be fairly high
(since `web-app` has to more or less proxy
all of the other operations in the system)
compared to the cost of dealing with special test tooling
in `frontend-adapter`.

## TODO

There's things this sample doesn't show yet, which I hope to add when there's time:

- **Message queues.** None of the interactions in the sample so far appear to be asynchronous
  (although some of them could be). Since this is a common use case, it'd be nice
  to have an example of modeling one and transitioning with it to various strategies.
