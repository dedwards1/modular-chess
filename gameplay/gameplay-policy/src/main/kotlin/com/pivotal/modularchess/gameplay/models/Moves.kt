package com.pivotal.modularchess.gameplay.models

import com.pivotal.modularchess.gameplay.models.ObjectiveSide.BLACK
import com.pivotal.modularchess.gameplay.models.ObjectiveSide.WHITE

sealed class Move {
    abstract val targetPosition: Position
    abstract fun effects(boardState: ObjectiveBoardState): List<MoveEffect>

    operator fun invoke(startingBoardState: ObjectiveBoardState): ObjectiveBoardState {
        return effects(startingBoardState).fold(startingBoardState) { boardState, effect ->
            effect(boardState)
        }.copy(
                whoseTurn = when(startingBoardState.whoseTurn) {
                    WHITE -> BLACK
                    BLACK -> WHITE
                }
        )
    }

    fun targetsAlliedPiece(boardState: ObjectiveBoardState) =
            boardState.whoseTurn == boardState[targetPosition]?.side
}

data class BasicMove(
        val origin: Position,
        val destination: Position
): Move() {
    constructor(coords: Pair<Position, Position>): this(origin = coords.first, destination = coords.second)

    override val targetPosition = origin

    override fun effects(boardState: ObjectiveBoardState) = listOf(
            RemovePiece(
                    row = origin.row,
                    column = origin.column
            ),
            PlacePiece(
                    piece = boardState[origin] ?: throw RuntimeException("Tried to move from a space with no piece on it? move: $this board: $boardState"),
                    row = destination.row,
                    column = destination.column
            )
    )
}

data class Position(
        val row: Int,
        val column: Int
)

interface MoveEffect {
    operator fun invoke(startingBoardState: ObjectiveBoardState): ObjectiveBoardState
}

data class RemovePiece(val row: Int, val column: Int) : MoveEffect {
    override fun invoke(startingBoardState: ObjectiveBoardState): ObjectiveBoardState {
        return startingBoardState.copy(
                rows = startingBoardState.rows.replaceAtIndex(row,
                        startingBoardState.rows[row].replaceAtIndex(column, null)
                )
        )
    }
}

data class PlacePiece(val piece: ObjectivePiece, val row: Int, val column: Int) : MoveEffect {
    override fun invoke(startingBoardState: ObjectiveBoardState): ObjectiveBoardState {
        return startingBoardState.copy(
                rows = startingBoardState.rows.replaceAtIndex(row,
                        startingBoardState.rows[row].replaceAtIndex(column, piece)
                )
        )
    }
}

fun <E> List<E>.replaceAtIndex(targetIndex: Int, item: E): List<E> {
    return this.mapIndexed { index, original: E -> if (index == targetIndex) item else original }
}