package com.pivotal.modularchess.gameplay.models

enum class ObjectiveSide {
    WHITE,
    BLACK
}
