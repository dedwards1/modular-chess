package com.pivotal.modularchess.gameplay.ports.primary

import com.pivotal.modularchess.gameplay.models.ObjectiveSide.*
import com.pivotal.modularchess.gameplay.models.ObjectiveBoardState.Companion.rowOf
import com.pivotal.modularchess.gameplay.models.PieceType.*
import com.pivotal.modularchess.gameplay.models.GameId
import com.pivotal.modularchess.gameplay.models.ObjectiveBoardState
import com.pivotal.modularchess.gameplay.models.ObjectivePiece
import com.pivotal.modularchess.gameplay.models.PieceType
import com.pivotal.modularchess.gameplay.ports.secondary.GameRepository

interface GetBoardState {
    operator fun <T> invoke(
            gameId: GameId,
            outcome: Outcome<T>
    ): T

    interface Outcome<T> {
        fun success(boardState: ObjectiveBoardState): T
        fun noSuchBoard(): T
    }
}

class DefaultGetBoardState(private val gameRepo: GameRepository) : GetBoardState {
    override fun <T> invoke(gameId: GameId, outcome: GetBoardState.Outcome<T>): T {
        val game = gameRepo.findGame(gameId) ?: return outcome.noSuchBoard()

        val initialBoardState = ObjectiveBoardState(rows = initialPosition, whoseTurn = WHITE)

        val boardStateAfterMoves = game.moves.fold(initialBoardState) { boardState, move ->
            move(boardState)
        }

        return outcome.success(boardStateAfterMoves)
    }

    private val initialPosition = listOf(
            listOf(ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK).asBlackPieces(),
            rowOf { ObjectivePiece(BLACK, PAWN) },
            rowOf { null },
            rowOf { null },
            rowOf { null },
            rowOf { null },
            rowOf { ObjectivePiece(WHITE, PAWN) },
            listOf(ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK).asWhitePieces()
    ).reversed() // just to make the code visually resemble the board
    // while maintaining rank/row index similarity
    // (that is, row index goes up as rank goes up)
}

fun List<PieceType>.asWhitePieces() = this.map { ObjectivePiece(WHITE, it) }
fun List<PieceType>.asBlackPieces() = this.map { ObjectivePiece(BLACK, it) }
