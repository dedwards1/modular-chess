package com.pivotal.modularchess.gameplay.models

data class ObjectiveBoardState(
        val rows: List<List<ObjectivePiece?>>,
        val whoseTurn: ObjectiveSide
) {
    operator fun get(rank: Int): List<ObjectivePiece?> {
        return rows[rank-1]
    }

    operator fun get(position: Position): ObjectivePiece? {
        return rows[position.row][position.column]
    }

    companion object {
        fun rowOf(producer: () -> ObjectivePiece?) = listOf(
                producer.invoke(),
                producer.invoke(),
                producer.invoke(),
                producer.invoke(),
                producer.invoke(),
                producer.invoke(),
                producer.invoke(),
                producer.invoke()
        )
    }
}
