package com.pivotal.modularchess.gameplay.models

data class GameId(val value: String)