package com.pivotal.modularchess.gameplay.ports.secondary.fakes

import com.pivotal.modularchess.gameplay.models.GameId
import com.pivotal.modularchess.gameplay.models.Move
import com.pivotal.modularchess.gameplay.ports.secondary.Game
import com.pivotal.modularchess.gameplay.ports.secondary.GameRepository

class FakeGameRepository : GameRepository {
    private val games = mutableMapOf<GameId, Game>()

    override fun findGame(gameId: GameId): Game? {
        return games[gameId]
    }

    override fun saveGame(game: Game): GameId {
        games[game.id] = game
        return game.id
    }

    override fun addMoveToGame(gameId: GameId, move: Move): Game? {
        val game = games[gameId] ?: return null
        games[gameId] = game.copy(moves = game.moves + move)
        return games[gameId]
    }
}
