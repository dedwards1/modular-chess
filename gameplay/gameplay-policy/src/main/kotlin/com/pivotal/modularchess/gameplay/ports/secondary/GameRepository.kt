package com.pivotal.modularchess.gameplay.ports.secondary

import com.pivotal.modularchess.gameplay.models.GameId
import com.pivotal.modularchess.gameplay.models.Move

interface GameRepository {
    fun findGame(gameId: GameId): Game?
    fun saveGame(game: Game): GameId
    fun addMoveToGame(gameId: GameId, move: Move): Game?
}

data class Game(
        val id: GameId,
        val moves: List<Move>
)