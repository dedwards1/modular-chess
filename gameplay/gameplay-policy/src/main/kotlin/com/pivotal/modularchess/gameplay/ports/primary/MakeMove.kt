package com.pivotal.modularchess.gameplay.ports.primary

import com.pivotal.modularchess.gameplay.models.GameId
import com.pivotal.modularchess.gameplay.models.Move
import com.pivotal.modularchess.gameplay.models.ObjectiveBoardState
import com.pivotal.modularchess.gameplay.ports.secondary.GameRepository

interface MakeMove {
    operator fun <T> invoke(gameId: GameId, move: Move, outcome: Outcome<T>): T

    interface Outcome<T> {
        fun success(): T
        fun moveDoesNotTargetAlliedPiece(): T
        fun noSuchBoard(): T
    }
}

class DefaultMakeMove(
        private val getBoardState: GetBoardState,
        private val gameRepo: GameRepository
) : MakeMove {
    override fun <T> invoke(
            gameId: GameId,
            move: Move,
            outcome: MakeMove.Outcome<T>
    ): T {
        return getBoardState(gameId, object: GetBoardState.Outcome<T> {
            override fun success(boardState: ObjectiveBoardState): T {
                if (!move.targetsAlliedPiece(boardState)) {
                    return outcome.moveDoesNotTargetAlliedPiece()
                }

                gameRepo.addMoveToGame(gameId, move)
                return outcome.success()
            }

            override fun noSuchBoard(): T {
                return outcome.noSuchBoard()
            }
        })
    }
}