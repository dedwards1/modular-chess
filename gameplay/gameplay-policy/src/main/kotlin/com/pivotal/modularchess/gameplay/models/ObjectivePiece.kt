package com.pivotal.modularchess.gameplay.models

data class ObjectivePiece(
        val side: ObjectiveSide,
        val type: PieceType
)