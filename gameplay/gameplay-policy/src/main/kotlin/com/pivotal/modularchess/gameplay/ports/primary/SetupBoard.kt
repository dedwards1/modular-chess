package com.pivotal.modularchess.gameplay.ports.primary

import com.pivotal.modularchess.gameplay.models.GameId
import com.pivotal.modularchess.gameplay.ports.secondary.Game
import com.pivotal.modularchess.gameplay.ports.secondary.GameRepository

interface SetupBoard {
    operator fun invoke(gameId: GameId): GameId
}

class DefaultSetupBoard(
        private val gameRepo: GameRepository
) : SetupBoard {
    override fun invoke(gameId: GameId): GameId {
        return gameRepo.saveGame(Game(id = gameId, moves = emptyList()))
    }
}
