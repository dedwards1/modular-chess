package com.pivotal.modularchess.gameplay

import com.pivotal.modularchess.gameplay.portcontracts.GameRepositoryContract
import com.pivotal.modularchess.gameplay.ports.secondary.fakes.FakeGameRepository

class FakeGameRepositoryTest: GameRepositoryContract() {
    private val repo = FakeGameRepository()
    override fun repository() = repo
}