package com.pivotal.modularchess.gameplay

import com.pivotal.modularchess.gameplay.models.ObjectiveSide.*
import com.pivotal.modularchess.gameplay.models.PieceType.*
import com.pivotal.modularchess.gameplay.models.*
import com.pivotal.modularchess.gameplay.ports.primary.*
import com.pivotal.modularchess.gameplay.ports.secondary.GameRepository
import com.pivotal.modularchess.gameplay.ports.secondary.fakes.FakeGameRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

class GetBoardStateTest {
    private val gameRepo: GameRepository = FakeGameRepository()
    private val getBoardState: GetBoardState = DefaultGetBoardState(gameRepo)
    private val setupBoard: SetupBoard = DefaultSetupBoard(gameRepo)

    @Test
    fun `for a new board with no moves`() {
        val boardId = setupBoard(GameId(UUID.randomUUID().toString()))

        val initialBoardState = getBoardState(boardId, object : DummyGetBoardStateOutcome<ObjectiveBoardState> {
            override fun success(boardState: ObjectiveBoardState) = boardState
        })

        assertThat(initialBoardState[1].all { it?.side == WHITE }).isTrue()
        assertThat(initialBoardState[1].map { it?.type }).isEqualTo(listOf(
                ROOK,
                KNIGHT,
                BISHOP,
                QUEEN,
                KING,
                BISHOP,
                KNIGHT,
                ROOK
        ))
        assertThat(initialBoardState[2].all { it?.side == WHITE }).isTrue()
        assertThat(initialBoardState[2].all { it?.type == PAWN }).isTrue()

        assertThat(initialBoardState[3].all { it == null }).isTrue()
        assertThat(initialBoardState[4].all { it == null }).isTrue()
        assertThat(initialBoardState[5].all { it == null }).isTrue()
        assertThat(initialBoardState[6].all { it == null }).isTrue()

        assertThat(initialBoardState[7].all { it?.side == BLACK }).isTrue()
        assertThat(initialBoardState[7].all { it?.type == PAWN }).isTrue()

        assertThat(initialBoardState[8].all { it?.side == BLACK }).isTrue()
        assertThat(initialBoardState[8].map { it?.type }).isEqualTo(listOf(
                ROOK,
                KNIGHT,
                BISHOP,
                QUEEN,
                KING,
                BISHOP,
                KNIGHT,
                ROOK
        ))

        assertThat(initialBoardState.whoseTurn).isEqualTo(WHITE)
    }

    @Test
    fun `after an odd number of moves`() {
        val boardId = setupBoard(GameId(UUID.randomUUID().toString()))
        gameRepo.addMoveToGame(
                gameId = boardId,
                move = BasicMove(
                        origin = Position(
                                row = 1,
                                column = 4
                        ),
                        destination = Position(
                                row = 2,
                                column = 4
                        )
                )
        )

        val newBoardState = getBoardState(boardId, object : DummyGetBoardStateOutcome<ObjectiveBoardState> {
            override fun success(boardState: ObjectiveBoardState) = boardState
        })

        assertThat(newBoardState.rows).isEqualTo(listOf(
                listOf(ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK).asBlackPieces(),
                ObjectiveBoardState.rowOf { ObjectivePiece(BLACK, PAWN) },
                ObjectiveBoardState.rowOf { null },
                ObjectiveBoardState.rowOf { null },
                ObjectiveBoardState.rowOf { null },
                ObjectiveBoardState.rowOf { null }.replaceAtIndex(4, ObjectivePiece(WHITE, PAWN)),
                ObjectiveBoardState.rowOf { ObjectivePiece(WHITE, PAWN) }.replaceAtIndex(4, null),
                listOf(ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK).asWhitePieces()
        ).reversed())

        assertThat(newBoardState.whoseTurn).isEqualTo(BLACK)
    }

    @Test
    fun `when a piece has been captured`() {
        val boardId = setupBoard(GameId(UUID.randomUUID().toString()))
        gameRepo.addMoveToGame(
                gameId = boardId,
                move = BasicMove(
                        origin = Position(
                                row = 1,
                                column = 4
                        ),
                        destination = Position(
                                row = 6,
                                column = 4
                        )
                )
        )

        val newBoardState = getBoardState(boardId, object : DummyGetBoardStateOutcome<ObjectiveBoardState> {
            override fun success(boardState: ObjectiveBoardState) = boardState
        })

        assertThat(newBoardState.rows).isEqualTo(listOf(
                listOf(ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK).asBlackPieces(),
                ObjectiveBoardState.rowOf { ObjectivePiece(BLACK, PAWN) }.replaceAtIndex(4, ObjectivePiece(WHITE, PAWN)),
                ObjectiveBoardState.rowOf { null },
                ObjectiveBoardState.rowOf { null },
                ObjectiveBoardState.rowOf { null },
                ObjectiveBoardState.rowOf { null },
                ObjectiveBoardState.rowOf { ObjectivePiece(WHITE, PAWN) }.replaceAtIndex(4, null),
                listOf(ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK).asWhitePieces()
        ).reversed())
    }

    @Test
    fun `after an even number of moves`() {
        val boardId = setupBoard(GameId(UUID.randomUUID().toString()))
        gameRepo.addMoveToGame(
                gameId = boardId,
                move = BasicMove(
                        origin = Position(
                                row = 1,
                                column = 4
                        ),
                        destination = Position(
                                row = 2,
                                column = 4
                        )
                )
        )
        gameRepo.addMoveToGame(
                gameId = boardId,
                move = BasicMove(
                        origin = Position(
                                row = 6,
                                column = 4
                        ),
                        destination = Position(
                                row = 5,
                                column = 4
                        )
                )
        )

        val newBoardState = getBoardState(boardId, object : DummyGetBoardStateOutcome<ObjectiveBoardState> {
            override fun success(boardState: ObjectiveBoardState) = boardState
        })

        assertThat(newBoardState.whoseTurn).isEqualTo(WHITE)
    }

    @Test
    fun `for a nonexistent board id`() {
        // no setupBoard call

        assertThat(getBoardState(GameId("nonsense-board-id"), object : DummyGetBoardStateOutcome<String> {
            override fun noSuchBoard() = "nonsense"
        })).isEqualTo("nonsense")
    }
}
