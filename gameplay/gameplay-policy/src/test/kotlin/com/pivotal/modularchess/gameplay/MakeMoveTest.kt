package com.pivotal.modularchess.gameplay

import com.pivotal.modularchess.gameplay.models.*
import com.pivotal.modularchess.gameplay.ports.primary.DefaultGetBoardState
import com.pivotal.modularchess.gameplay.ports.primary.DefaultMakeMove
import com.pivotal.modularchess.gameplay.ports.primary.DefaultSetupBoard
import com.pivotal.modularchess.gameplay.ports.primary.MakeMove
import com.pivotal.modularchess.gameplay.ports.secondary.fakes.FakeGameRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

class MakeMoveTest {
    private val gameRepo = FakeGameRepository()
    private val getBoardState = DefaultGetBoardState(gameRepo)
    private val setupBoard = DefaultSetupBoard(gameRepo)
    private val gameId = setupBoard(GameId(UUID.randomUUID().toString()))

    private val makeMove: MakeMove = DefaultMakeMove(getBoardState, gameRepo)

    @Test
    fun `successfully making a move`() {
        val move = BasicMove(
                origin = Position(row = 0, column = 1),
                destination = Position(row = 2, column = 0)
        )

        assertThat(gameRepo.findGame(gameId)?.moves?.size).isEqualTo(0)

        val success = makeMove(gameId, move, object : DummyMakeMoveOutcome<String> {
            override fun success() = "you did it"
        })

        assertThat(gameRepo.findGame(gameId)?.moves?.size).isEqualTo(1)
        assertThat(gameRepo.findGame(gameId)?.moves?.get(0)).isEqualTo(move)
        assertThat(success).isEqualTo("you did it")
    }

    @Test
    fun `when the target piece is not owned by the player whose turn it is`() {
        val move = BasicMove(
                origin = Position(row = 7, column = 1),
                destination = Position(row = 5, column = 0)
        )

        assertThat(gameRepo.findGame(gameId)?.moves?.size).isEqualTo(0)

        val illegal = makeMove(gameId, move, object : DummyMakeMoveOutcome<String> {
            override fun moveDoesNotTargetAlliedPiece() = "that's not yours"
        })

        assertThat(gameRepo.findGame(gameId)?.moves?.size).isEqualTo(0)
        assertThat(illegal).isEqualTo("that's not yours")
    }

    @Test
    fun `for a nonexistent board id`() {
        val move = BasicMove(
                origin = Position(row = 7, column = 1),
                destination = Position(row = 5, column = 0)
        )

        val result = makeMove(GameId("not a real board id"), move, object : DummyMakeMoveOutcome<String> {
            override fun noSuchBoard() = "that's not a thing"
        })

        assertThat(gameRepo.findGame(gameId)?.moves?.size).isEqualTo(0)
        assertThat(result).isEqualTo("that's not a thing")
    }
}