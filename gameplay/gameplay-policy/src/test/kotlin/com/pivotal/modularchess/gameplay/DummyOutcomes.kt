package com.pivotal.modularchess.gameplay

import com.pivotal.modularchess.gameplay.models.ObjectiveBoardState
import com.pivotal.modularchess.gameplay.ports.primary.GetBoardState
import com.pivotal.modularchess.gameplay.ports.primary.MakeMove

interface DummyGetBoardStateOutcome<T>: GetBoardState.Outcome<T> {
    override fun success(boardState: ObjectiveBoardState): T = throw RuntimeException("'success' outcome should not have been invoked!")
    override fun noSuchBoard(): T = throw RuntimeException("'noSuchBoard' outcome should not have been invoked!")
}

interface DummyMakeMoveOutcome<T> : MakeMove.Outcome<T> {
    override fun success(): T = throw RuntimeException("success outcome should not have been invoked!")
    override fun moveDoesNotTargetAlliedPiece(): T = throw RuntimeException("moveDoesNotTargetAlliedPiece outcome should not have been called")
    override fun noSuchBoard(): T = throw RuntimeException("noSuchBoard outcome should not have been called")
}