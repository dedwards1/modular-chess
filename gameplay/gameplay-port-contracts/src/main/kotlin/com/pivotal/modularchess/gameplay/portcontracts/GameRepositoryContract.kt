package com.pivotal.modularchess.gameplay.portcontracts

import com.pivotal.modularchess.gameplay.models.*
import com.pivotal.modularchess.gameplay.ports.secondary.Game
import com.pivotal.modularchess.gameplay.ports.secondary.GameRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

abstract class GameRepositoryContract {
    abstract fun repository(): GameRepository

    private val sampleMoveHistory1 = listOf(
            BasicMove(Position(1,1) to Position(2,1)),
            BasicMove(Position(7,1) to Position(5,0))
    )

    private val sampleMoveHistory2 = listOf(
            BasicMove(Position(1,4) to Position(3,4)),
            BasicMove(Position(6,4) to Position(4,4))
    )

    @Test
    fun `fetching ids that have not previously been saved returns null`() {
        assertThat(repository().findGame(GameId(UUID.randomUUID().toString()))).isNull()
    }

    @Test
    fun `fetching a previously saved history`() {
        val game1 = Game(id = GameId(UUID.randomUUID().toString()), moves = sampleMoveHistory1)
        val game2 = Game(id = GameId(UUID.randomUUID().toString()), moves = sampleMoveHistory2)

        repository().saveGame(game1)
        repository().saveGame(game2)

        assertThat(repository().findGame(game1.id)).isEqualTo(game1)
        assertThat(repository().findGame(game2.id)).isEqualTo(game2)
    }

    @Test
    fun `appending new moves`() {
        val id = GameId(UUID.randomUUID().toString())
        repository().saveGame(Game(id = id, moves = sampleMoveHistory1))
        val newMove = BasicMove(Position(1, 7) to Position(2, 7))

        repository().addMoveToGame(id, newMove)

        assertThat(repository().findGame(id)).isEqualTo(
                Game(
                        id = id,
                        moves = sampleMoveHistory1 + newMove
                )
        )
    }
}