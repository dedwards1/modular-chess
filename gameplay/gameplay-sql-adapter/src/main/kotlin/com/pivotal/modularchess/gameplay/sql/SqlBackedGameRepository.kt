package com.pivotal.modularchess.gameplay.sql

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.pivotal.modularchess.gameplay.models.BasicMove
import com.pivotal.modularchess.gameplay.models.GameId
import com.pivotal.modularchess.gameplay.models.Move
import com.pivotal.modularchess.gameplay.models.Position
import com.pivotal.modularchess.gameplay.ports.secondary.Game
import com.pivotal.modularchess.gameplay.ports.secondary.GameRepository
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import java.sql.ResultSet

class SqlBackedGameRepository(
        private val jdbcTemplate: JdbcTemplate
) : GameRepository {
    override fun findGame(gameId: GameId): Game? {
        return try {
            jdbcTemplate.queryForObject(
                    "SELECT * FROM gameplay__games WHERE id = ?;",
                    arrayOf(gameId.value),
                    { resultSet: ResultSet, rowNum: Int ->
                        Game(
                                id = GameId(resultSet.getString("id")),
                                moves = deserializeMoves(resultSet.getString("moves"))
                        )
                    }
            )
        } catch(e: EmptyResultDataAccessException) {
            null
        }
    }

    override fun saveGame(game: Game): GameId {
        SimpleJdbcInsert(jdbcTemplate.dataSource!!)
                .withTableName("gameplay__games")
                .execute(mapOf(
                        "id" to game.id.value,
                        "moves" to serializeMoves(game.moves)
                ))

        return game.id
    }

    override fun addMoveToGame(gameId: GameId, move: Move): Game? {
        val game = findGame(gameId) ?: return null
        val newMoves = game.moves + move
        jdbcTemplate.update(
                "UPDATE gameplay__games SET moves = ? WHERE id = ?",
                serializeMoves(newMoves),
                gameId.value
        )
        return game.copy(moves = newMoves)
    }

    private fun serializedMove(type: String, details: Map<String, Any>) =
            mapOf("type" to type, "details" to details)

    private fun serializeMoves(moves: List<Move>): String {
        return ObjectMapper().writeValueAsString(moves.map { move ->
            when(move) {
                is BasicMove -> serializedMove("basic", serializeBasicMove(move))
            }
        })
    }

    private fun deserializeMoves(json: String): List<Move> {
        return ObjectMapper().readTree(json).map { move ->
            when(move.get("type").textValue()) {
                "basic" -> deserializeBasicMove(move.get("details"))
                else -> throw RuntimeException("I don't know how to deserialize a move of type ${move.get("type").textValue()}")
            }
        }
    }

    private fun serializeBasicMove(move: BasicMove) = mapOf(
            "from" to "${move.origin.row},${move.origin.column}",
            "to" to "${move.destination.row},${move.destination.column}"
    )

    private fun deserializeBasicMove(json: JsonNode) = BasicMove(
            origin = json.get("from").textValue()
                    .split(",")
                    .let { Position(row = it[0].toInt(), column = it[1].toInt()) },
            destination = json.get("to").textValue()
                    .split(",")
                    .let { Position(row = it[0].toInt(), column = it[1].toInt()) }
    )
}