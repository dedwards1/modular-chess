CREATE TABLE IF NOT EXISTS gameplay__games
(
    id VARCHAR(40) PRIMARY KEY NOT NULL,
    moves TEXT NOT NULL
);