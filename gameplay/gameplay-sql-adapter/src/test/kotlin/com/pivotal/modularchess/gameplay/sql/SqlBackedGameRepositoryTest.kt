package com.pivotal.modularchess.gameplay.sql

import com.pivotal.modularchess.gameplay.portcontracts.GameRepositoryContract
import com.pivotal.modularchess.gameplay.ports.secondary.GameRepository
import org.junit.jupiter.api.Disabled
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource

@SpringBootTest(
        classes = [ SqlBackedGameRepositoryTestConfig::class ]
)
@ActiveProfiles("SqlBackedGameRepositoryTest")
@TestPropertySource(properties = [
    // This tells h2 to behave more like postgres,
    // which we'll pretend is what we're using in production
    "spring.datasource.url=jdbc:h2:mem:~/test;MODE=PostgreSQL"
])
class SqlBackedGameRepositoryTest: GameRepositoryContract() {
    @Autowired
    private lateinit var repo: SqlBackedGameRepository

    override fun repository() = repo
}

@Configuration
@EnableAutoConfiguration
@Profile("SqlBackedGameRepositoryTest")
@Import(
        SqlBackedGameRepository::class
)
class SqlBackedGameRepositoryTestConfig
