package com.pivotal.modularchess.gameplay.api

import com.pivotal.modularchess.gameplay.models.BasicMove
import com.pivotal.modularchess.gameplay.models.GameId
import com.pivotal.modularchess.gameplay.models.Position
import com.pivotal.modularchess.gameplay.ports.primary.MakeMove
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.http.ResponseEntity.unprocessableEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

data class MakeMoveRequest(
        val gameId: String = "",
        val from: String = "",
        val to: String = ""
)

class MakeMoveResponses: MakeMove.Outcome<ResponseEntity<*>> {
    override fun success(): ResponseEntity<*> {
        return ok(emptyMap<String,String>())
    }

    override fun moveDoesNotTargetAlliedPiece(): ResponseEntity<*> {
        return unprocessableEntity().body(mapOf(
                "message" to "move does not target an allied piece"
        ))
    }

    override fun noSuchBoard(): ResponseEntity<*> {
        return unprocessableEntity().body(mapOf(
                "message" to "no such game"
        ))
    }
}

@RestController
class MakeMoveEndpoint(
        private val makeMove: MakeMove
) {

    @PostMapping("/api/v1/moves")
    fun handleMakeMove(@RequestBody request: MakeMoveRequest): ResponseEntity<*> {
        return makeMove(
                gameId = GameId(request.gameId),
                move = BasicMove(
                        origin = convertPosition(request.from),
                        destination = convertPosition(request.to)
                ),
                outcome = MakeMoveResponses()
        )
    }

    fun convertPosition(spaceNotation: String): Position {
        val position = Position(
                row = spaceNotation[1].toString().toInt() - 1,
                column = listOf('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h').indexOf(spaceNotation[0])
        )
        return position
    }
}