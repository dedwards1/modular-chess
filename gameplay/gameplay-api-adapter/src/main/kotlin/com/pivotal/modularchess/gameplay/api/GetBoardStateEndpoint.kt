package com.pivotal.modularchess.gameplay.api

import com.pivotal.modularchess.gameplay.models.GameId
import com.pivotal.modularchess.gameplay.models.ObjectiveBoardState
import com.pivotal.modularchess.gameplay.models.ObjectiveSide
import com.pivotal.modularchess.gameplay.models.ObjectiveSide.BLACK
import com.pivotal.modularchess.gameplay.models.ObjectiveSide.WHITE
import com.pivotal.modularchess.gameplay.models.PieceType
import com.pivotal.modularchess.gameplay.models.PieceType.*
import com.pivotal.modularchess.gameplay.ports.primary.GetBoardState
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.unprocessableEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

class GetBoardStateResponses: GetBoardState.Outcome<ResponseEntity<*>> {
    override fun success(boardState: ObjectiveBoardState): ResponseEntity<*> {
        return ResponseEntity.ok(mapOf(
                "whoseTurn" to when(boardState.whoseTurn) {
                    WHITE -> "white"
                    BLACK -> "black"
                },
                "rows" to boardState.rows.map { row ->
                    row.map { if(it == null) null else mapOf(
                            "side" to codeFor(it.side),
                            "type" to codeFor(it.type))
                    }
                }
        ))
    }

    override fun noSuchBoard(): ResponseEntity<*> {
        return unprocessableEntity().body(mapOf("message" to "no such board"))
    }

    fun codeFor(pieceType: PieceType) = when(pieceType) {
        ROOK -> "R"
        KNIGHT -> "N"
        BISHOP -> "B"
        KING -> "K"
        QUEEN -> "Q"
        PAWN -> ""
    }

    fun codeFor(side: ObjectiveSide) = when(side) {
        BLACK -> "black"
        WHITE -> "white"
    }
}

@RestController
class GetBoardStateEndpoint(
        private val getBoardState: GetBoardState
) {
    @GetMapping("/api/v1/boardState/{gameId}")
    fun handleGetBoardState(@PathVariable("gameId") gameId: String): ResponseEntity<*> {
        return getBoardState(gameId = GameId(gameId), outcome = GetBoardStateResponses())
    }
}