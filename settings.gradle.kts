rootProject.name = "modularity-sample"

include(
        "deployables:chess-app",
        
        "gameplay:gameplay-policy",
        "gameplay:gameplay-port-contracts",
        "gameplay:gameplay-api-adapter",
        "gameplay:gameplay-sql-adapter",

        "organizing-games:organizing-games-policy",
        "organizing-games:organizing-games-port-contracts",
        "organizing-games:organizing-games-gameplay-adapter",
        "organizing-games:organizing-games-api-adapter",
        "organizing-games:organizing-games-sql-adapter",

        "web-app:frontend-adapter",
        "web-app:browser-client"
)