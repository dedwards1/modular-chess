CREATE TABLE IF NOT EXISTS organizing_games__players
(
    id VARCHAR(40) PRIMARY KEY NOT NULL,
    email VARCHAR(100)
);