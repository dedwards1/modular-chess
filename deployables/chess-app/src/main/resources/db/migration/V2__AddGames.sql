CREATE TABLE IF NOT EXISTS organizing_games__games
(
    id VARCHAR(40) PRIMARY KEY NOT NULL,
    guest_id VARCHAR(40) NOT NULL,
    host_id VARCHAR(40) NOT NULL,
    host_side CHAR(5) NOT NULL,
    invitation_status VARCHAR(20) NOT NULL
);