package com.pivotal.modularchess.chessapp

import com.pivotal.modularchess.frontend.FrontendFacade
import com.pivotal.modularchess.gameplay.api.GetBoardStateEndpoint
import com.pivotal.modularchess.gameplay.api.MakeMoveEndpoint
import com.pivotal.modularchess.gameplay.ports.primary.DefaultGetBoardState
import com.pivotal.modularchess.gameplay.ports.primary.DefaultMakeMove
import com.pivotal.modularchess.gameplay.ports.primary.DefaultSetupBoard
import com.pivotal.modularchess.organizinggames.api.*
import com.pivotal.modularchess.organizinggames.gameplayadapter.InitializeGameViaSetupBoard
import com.pivotal.modularchess.organizinggames.ports.primary.DefaultCreateGame
import com.pivotal.modularchess.organizinggames.ports.primary.DefaultCreatePlayer
import com.pivotal.modularchess.organizinggames.ports.primary.DefaultGetPlayersGames
import com.pivotal.modularchess.organizinggames.ports.primary.DefaultRespondToInvitation
import com.pivotal.modularchess.organizinggames.sql.SqlBackedPlayerRepository
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.http.client.ClientHttpResponse
import org.springframework.web.client.DefaultResponseErrorHandler
import org.springframework.web.client.RestTemplate

@SpringBootApplication
@Import(
		DefaultMakeMove::class,
		MakeMoveEndpoint::class,

		DefaultSetupBoard::class,

		DefaultGetBoardState::class,
		GetBoardStateEndpoint::class,

		DefaultCreatePlayer::class,
		CreatePlayerEndpoint::class,
		GetPlayersEndpoint::class,

		DefaultCreateGame::class,
		CreateGameEndpoint::class,

		GetGameEndpoint::class,

		DefaultGetPlayersGames::class,
		GetPlayersGamesEndpoint::class,

		DefaultRespondToInvitation::class,
		RespondToInvitationEndpoint::class,

		SqlBackedPlayerRepository::class,
		com.pivotal.modularchess.organizinggames.sql.SqlBackedGameRepository::class,
		com.pivotal.modularchess.gameplay.sql.SqlBackedGameRepository::class,

		InitializeGameViaSetupBoard::class
)
@EnableJpaRepositories(
		"com.pivotal.modularchess.organizinggames.sql",
		"com.pivotal.modularchess.gameplay.sql"
)
@EntityScan(
		"com.pivotal.modularchess.organizinggames.sql",
		"com.pivotal.modularchess.gameplay.sql"
)
class ModularitySampleApplication {
	@Bean
	fun frontendFacade(
			@Qualifier("proxying-resttemplate") restTemplate: RestTemplate,
			@Value("\${facade.url.createGame}") createGameUrl: String,
			@Value("\${facade.url.createPlayer}") createPlayerUrl: String,
			@Value("\${facade.url.respondToInvitation}") respondToInvitationUrl: String,
			@Value("\${facade.url.getBoardState}") getBoardStateUrl: String,
			@Value("\${facade.url.makeMove}") makeMoveUrl: String,
			@Value("\${facade.url.getPlayers}") getPlayersUrl: String,
			@Value("\${facade.url.getGamesForPlayer}") getGamesForPlayerUrl: String,
			@Value("\${facade.url.getGame}") getGameUrl: String
	) = FrontendFacade(
			restTemplate = restTemplate,
			createGameUrl = createGameUrl,
			createPlayerUrl = createPlayerUrl,
			respondToInvitationUrl = respondToInvitationUrl,
			getBoardStateUrl = getBoardStateUrl,
			makeMoveUrl = makeMoveUrl,
			getPlayersUrl = getPlayersUrl,
			getGamesForPlayerUrl = getGamesForPlayerUrl,
			getGameUrl = getGameUrl
	)

	@Bean
	@Qualifier("proxying-resttemplate")
	// This RestTemplate will not throw exceptions on non-2xx responses,
	// allowing them to be proxied just like success responses.
	fun restTemplate(): RestTemplate {
		val restTemplate = RestTemplate()
		restTemplate.errorHandler = object : DefaultResponseErrorHandler() {
			override fun hasError(response: ClientHttpResponse) = false
		}
		return restTemplate
	}
}

fun main(args: Array<String>) {
	runApplication<ModularitySampleApplication>(*args)
}
