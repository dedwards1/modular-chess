package com.pivotal.modularchess.organizinggames.portcontracts

import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

abstract class PlayerRepositoryContract {
    abstract fun repository(): PlayerRepository

    private val unsavedPlayer1 = Player(email = "${UUID.randomUUID().toString()}@example.com")
    private val unsavedPlayer2 = Player(email = "${UUID.randomUUID().toString()}@example.com")

    @Test
    fun `saving a player`() {
        val savedPlayer1 = repository().savePlayer(unsavedPlayer1)
        val savedPlayer2 = repository().savePlayer(unsavedPlayer2)

        assertThat(savedPlayer1.id).isNotEqualTo(savedPlayer2.id)

        assertThat(unsavedPlayer1.copy(id = savedPlayer1.id)).isEqualTo(savedPlayer1)
        assertThat(unsavedPlayer2.copy(id = savedPlayer2.id)).isEqualTo(savedPlayer2)

        assertThat(repository().findPlayer(savedPlayer1.id!!)).isEqualTo(savedPlayer1)
        assertThat(repository().findPlayer(savedPlayer2.id!!)).isEqualTo(savedPlayer2)
    }

    @Test
    fun `finding a player by email`() {
        val savedPlayer1 = repository().savePlayer(unsavedPlayer1)
        assertThat(repository().findPlayerByEmail(savedPlayer1.email)).isEqualTo(savedPlayer1)
        assertThat(repository().findPlayerByEmail(unsavedPlayer2.email)).isNull()
    }

    @Test
    fun `finding all players`() {
        val savedPlayer1 = repository().savePlayer(unsavedPlayer1)
        val savedPlayer2 = repository().savePlayer(unsavedPlayer2)

        assertThat(repository().findAllPlayers()).containsExactlyInAnyOrder(savedPlayer1, savedPlayer2)
    }
}