package com.pivotal.modularchess.organizinggames.portcontracts

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.InvitationStatus.*
import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.models.Side.BLACK
import com.pivotal.modularchess.organizinggames.models.Side.WHITE
import com.pivotal.modularchess.organizinggames.ports.secondary.GameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

abstract class GameRepositoryContract {
    abstract fun playerRepository(): PlayerRepository
    abstract fun gameRepository(): GameRepository

    fun randomPlayer() =
            playerRepository().savePlayer(Player(email = "${UUID.randomUUID()}@example.com"))

    @Test
    fun `saving a game`() {
        val playerA = randomPlayer()
        val playerB = randomPlayer()

        val unsavedGame1 = Game(
                host = playerA,
                guest = playerB,
                hostSide = WHITE,
                invitationStatus = PENDING
        )
        val game1 = gameRepository().saveGame(unsavedGame1)

        val unsavedGame2 = Game(
                host = playerA,
                guest = playerB,
                hostSide = BLACK,
                invitationStatus = ACCEPTED
        )
        val game2 = gameRepository().saveGame(unsavedGame2)

        assertThat(game1.id).isNotEqualTo(game2.id)

        assertThat(unsavedGame1.copy(id = game1.id)).isEqualTo(game1)
        assertThat(unsavedGame2.copy(id = game2.id)).isEqualTo(game2)

        assertThat(gameRepository().findGame(game1.id!!)).isEqualTo(game1)
        assertThat(gameRepository().findGame(game2.id!!)).isEqualTo(game2)
    }

    @Test
    fun `updating a game`() {
        val playerA = randomPlayer()
        val playerB = randomPlayer()

        val originalGame = gameRepository().saveGame(Game(
                host = playerA,
                guest = playerB,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        val modifiedGame = originalGame.copy(
                invitationStatus = ACCEPTED
        )

        val updatedGame = gameRepository().saveGame(modifiedGame)

        assertThat(gameRepository().findGame(originalGame.id!!)).isEqualTo(modifiedGame)
        assertThat(updatedGame).isEqualTo(modifiedGame)
    }

    @Test
    fun `fetching games for a player`() {
        val playerA = randomPlayer()
        val playerB = randomPlayer()
        val playerC = randomPlayer()

        val hostedGame = gameRepository().saveGame(Game(
                host = playerA,
                guest = playerB,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        val guestGame = gameRepository().saveGame(Game(
                host = playerB,
                guest = playerA,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        val otherGame = gameRepository().saveGame(Game(
                host = playerC,
                guest = playerB,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        val gamesForPlayerA = gameRepository().findGamesForPlayer(playerA.id!!)

        assertThat(gamesForPlayerA).contains(hostedGame, guestGame)
        assertThat(gamesForPlayerA).doesNotContain(otherGame)
    }
}