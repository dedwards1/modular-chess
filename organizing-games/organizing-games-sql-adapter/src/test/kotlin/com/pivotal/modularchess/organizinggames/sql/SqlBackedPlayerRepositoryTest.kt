package com.pivotal.modularchess.organizinggames.sql

import com.pivotal.modularchess.organizinggames.portcontracts.PlayerRepositoryContract
import org.junit.jupiter.api.AfterEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource

@SpringBootTest(
        classes = [ TestConfig::class ]
)
@TestPropertySource(properties = [
    // This tells h2 to behave more like postgres,
    // which we'll pretend is what we're using in production
    "spring.datasource.url=jdbc:h2:mem:~/test;MODE=PostgreSQL",

    // We disable automatic schema generation to force the tests
    // to rely on the schema defined in our resources/schema.sql file.
    // When we write database migrations for our deployable module,
    // we can refer to this schema file to tell us our target schema.
    // (And since the schema file is in our test resources,
    // it won't interfere with any modules that depend on this one.)
    "spring.jpa.generate-ddl=false",
    "spring.jpa.hibernate.ddl-auto=none"
])
class SqlBackedPlayerRepositoryTest: PlayerRepositoryContract() {
    @Autowired
    private lateinit var repo: SqlBackedPlayerRepository

    @Autowired
    private lateinit var jpaRepo: PlayerJpaRepository

    override fun repository() = repo

    @AfterEach
    fun cleanup() {
        jpaRepo.deleteAll()
    }
}