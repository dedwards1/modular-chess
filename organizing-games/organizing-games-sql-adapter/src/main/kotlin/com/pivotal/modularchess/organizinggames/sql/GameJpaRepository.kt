package com.pivotal.modularchess.organizinggames.sql

import com.pivotal.modularchess.organizinggames.models.Game
import org.springframework.data.repository.CrudRepository

interface GameJpaRepository: CrudRepository<GameDbRecord, String> {
    fun findByHostId(value: String): List<GameDbRecord>
    fun findByGuestId(value: String): List<GameDbRecord>
}
