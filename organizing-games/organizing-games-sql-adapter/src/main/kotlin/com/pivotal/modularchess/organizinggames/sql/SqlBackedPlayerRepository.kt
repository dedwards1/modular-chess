package com.pivotal.modularchess.organizinggames.sql

import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository
import java.util.*

class SqlBackedPlayerRepository(
        private val jpaRepository: PlayerJpaRepository
): PlayerRepository {
    override fun findPlayer(id: PlayerId): Player? {
        return jpaRepository.findById(id.value).map { it.toPlayer() }.orElse(null)
    }

    override fun savePlayer(player: Player): Player {
        return jpaRepository.save(
                player.copy(
                        id = player.id ?: PlayerId(UUID.randomUUID().toString())
                ).let { PlayerDbRecord(it) }
        ).toPlayer()
    }

    override fun findPlayerByEmail(email: String): Player? {
        return jpaRepository.findByEmail(email).map { it.toPlayer() }.orElse(null)
    }

    override fun findAllPlayers(): List<Player> {
        return jpaRepository.findAll().map { it.toPlayer() }
    }

}
