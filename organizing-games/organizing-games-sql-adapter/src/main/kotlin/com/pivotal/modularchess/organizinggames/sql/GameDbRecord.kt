package com.pivotal.modularchess.organizinggames.sql

import com.pivotal.modularchess.organizinggames.models.*
import com.pivotal.modularchess.organizinggames.models.InvitationStatus.*
import com.pivotal.modularchess.organizinggames.models.Side.*
import java.lang.RuntimeException
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity(name = "organizing_games__games")
data class GameDbRecord(
    @Id
    var id: String? = null,

    @ManyToOne
    var host: PlayerDbRecord? = null,

    @ManyToOne
    var guest: PlayerDbRecord? = null,

    var hostSide: String = "",

    var invitationStatus: String = ""

) {
    fun toGame(): Game {
        return Game(
                id = this.id?.let { GameId(it) },
                host = this.host?.toPlayer() ?: throw RuntimeException("Somehow this game record didn't have a host?!?"),
                guest = this.guest?.toPlayer() ?: throw RuntimeException("Somehow this game record didn't have a guest?!?"),
                hostSide = when(this.hostSide) {
                    "black" -> BLACK
                    "white" -> WHITE
                    else -> throw RuntimeException("Unknown hostSide value on game record ${this.id}: ${this.hostSide}")
                },
                invitationStatus = when(this.invitationStatus) {
                    "pending" -> PENDING
                    "accepted" -> ACCEPTED
                    "declined" -> DECLINED
                    else -> throw RuntimeException("Unknown invitationStatus value on game record ${this.id}: ${this.invitationStatus}")
                }
        )
    }

    constructor(game: Game) : this(
            id = game.id?.value,
            host = PlayerDbRecord(game.host),
            guest = PlayerDbRecord(game.guest),
            hostSide = when(game.hostSide) {
                BLACK -> "black"
                WHITE -> "white"
            },
            invitationStatus = when(game.invitationStatus) {
                PENDING -> "pending"
                ACCEPTED -> "accepted"
                DECLINED -> "declined"
            }
    )
}