package com.pivotal.modularchess.organizinggames.sql

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.GameId
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.ports.secondary.GameRepository
import java.util.*

class SqlBackedGameRepository(
        private val jpaRepository: GameJpaRepository
) : GameRepository {
    override fun saveGame(game: Game): Game {
        val savedGame = jpaRepository.save(GameDbRecord(game.copy(
                id = game.id ?: GameId(UUID.randomUUID().toString())
        ))).toGame()
        return savedGame
    }

    override fun findGame(id: GameId): Game? {
        return jpaRepository.findById(id.value).map { it.toGame() }.orElse(null)
    }

    override fun findGamesForPlayer(id: PlayerId): List<Game> {
        val allGameRecords =
                jpaRepository.findByHostId(id.value) + jpaRepository.findByGuestId(id.value)
        return allGameRecords.map { it.toGame() }
    }

}
