package com.pivotal.modularchess.organizinggames.sql

import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.models.PlayerId
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = "organizing_games__players")
data class PlayerDbRecord(
        @Id
        var id: String? = null,
        var email: String = ""
) {
    fun toPlayer() = Player(
            id = this.id?.let { PlayerId(it) },
            email = this.email
    )

    constructor(player: Player): this(
            id = player.id?.value,
            email = player.email
    )
}
