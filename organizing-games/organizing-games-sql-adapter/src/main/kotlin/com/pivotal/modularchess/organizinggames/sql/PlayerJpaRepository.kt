package com.pivotal.modularchess.organizinggames.sql

import org.springframework.data.repository.CrudRepository
import java.util.*

interface PlayerJpaRepository: CrudRepository<PlayerDbRecord, String> {
    fun findByEmail(email: String): Optional<PlayerDbRecord>
}
