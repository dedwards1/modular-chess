package com.pivotal.modularchess.organizinggames.api

import com.pivotal.modularchess.organizinggames.models.*
import com.pivotal.modularchess.organizinggames.models.Side.BLACK
import com.pivotal.modularchess.organizinggames.ports.primary.CreateGame
import io.restassured.RestAssured
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles

@SpringBootTest(
        classes = [CreateGameEndpointTestConfig::class],
        webEnvironment = RANDOM_PORT
)
@ActiveProfiles("CreateGameEndpointTest")
class CreateGameEndpointTest {

    @LocalServerPort
    private lateinit var port: String

    private val baseUrl: String
        get() = "http://localhost:$port"

    @Autowired
    private lateinit var createGameSpy: CreateGameSpy

    @Test
    fun success() {
        RestAssured
                .given()
                .contentType("application/json")
                .body("""
                    {
                        "host": "judit",
                        "guest": "garry",
                        "hostSide": "white"
                    }
                """.trimIndent())
                .`when`()
                .post("$baseUrl/api/v1/games")
                .then()
                .statusCode(200)
                .body("id", equalTo("the-game-id"))
                .body("host", equalTo("host-player-id"))
                .body("guest", equalTo("guest-player-id"))
                .body("hostSide", equalTo("black"))
                .body("invitationStatus", equalTo("pending"))

        assertThat(createGameSpy.lastInvocation).isEqualTo(Triple(
                PlayerId("judit"),
                PlayerId("garry"),
                Side.WHITE
        ))
    }

    @Test
    fun noSuchPlayer() {
        createGameSpy.outcome = { it.noSuchPlayer(PlayerId("some-player")) }

        RestAssured
                .given()
                .contentType("application/json")
                .body("""
                    {
                        "host": "judit",
                        "guest": "garry",
                        "hostSide": "white"
                    }
                """.trimIndent())
                .`when`()
                .post("$baseUrl/api/v1/games")
                .then()
                .statusCode(422)
                .body("message", equalTo("No such player: some-player"))
    }
}

class CreateGameSpy : CreateGame {
    var lastInvocation: Triple<PlayerId, PlayerId, Side>? = null
    var outcome: (CreateGame.Outcome<*>) -> Any? = {
        it.success(Game(
                id = GameId("the-game-id"),
                host = Player(id = PlayerId("host-player-id"), email = "host-player-email"),
                guest = Player(id = PlayerId("guest-player-id"), email = "guest-player-email"),
                hostSide = BLACK,
                invitationStatus = InvitationStatus.PENDING
        ))
    }

    override fun <T> invoke(hostId: PlayerId, guestId: PlayerId, hostSide: Side, outcome: CreateGame.Outcome<T>): T {
        lastInvocation = Triple(hostId, guestId, hostSide)
        @Suppress("UNCHECKED_CAST")
        return this.outcome(outcome) as T
    }
}

@Configuration
@EnableAutoConfiguration
@Profile("CreateGameEndpointTest")
@Import(
        CreateGameSpy::class,
        CreateGameEndpoint::class
)
class CreateGameEndpointTestConfig