package com.pivotal.modularchess.organizinggames.api

import com.pivotal.modularchess.organizinggames.models.*
import com.pivotal.modularchess.organizinggames.models.Side.BLACK
import com.pivotal.modularchess.organizinggames.ports.primary.RespondToInvitation
import io.restassured.RestAssured
import io.restassured.response.Response
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles

@SpringBootTest(
        classes = [RespondToInvitationEndpointTestConfig::class],
        webEnvironment = RANDOM_PORT
)
@ActiveProfiles("RespondToInvitationEndpointTest")
class RespondToInvitationEndpointTest {

    @LocalServerPort
    private lateinit var port: String

    private val baseUrl: String
        get() = "http://localhost:$port"

    @Autowired
    private lateinit var respondToInvitationSpy: RespondToInvitationSpy

    @Test
    fun success() {
        respondToInvitationSpy.outcome = {
            it.success(Game(
                    id = GameId("the-game-id"),
                    host = Player(id = PlayerId("host-player-id"), email = "host-player-email"),
                    guest = Player(id = PlayerId("guest-player-id"), email = "guest-player-email"),
                    hostSide = BLACK,
                    invitationStatus = InvitationStatus.ACCEPTED
            ))
        }

        makeRequestWith("""
                    {
                        "guest": "garry",
                        "game": "the-game-id",
                        "accept": true
                    }
                """.trimIndent())
                .then()
                .statusCode(200)
                .body("id", equalTo("the-game-id"))
                .body("host", equalTo("host-player-id"))
                .body("guest", equalTo("guest-player-id"))
                .body("hostSide", equalTo("black"))
                .body("invitationStatus", equalTo("accepted"))

        assertThat(respondToInvitationSpy.lastInvocation).isEqualTo(Triple(
                PlayerId("garry"),
                GameId("the-game-id"),
                true
        ))
    }

    @Test
    fun noSuchPlayer() {
        respondToInvitationSpy.outcome = {
            it.noSuchPlayer(PlayerId("no-such-player"))
        }

        makeRequest()
                .then()
                .statusCode(422)
                .body("message", equalTo("No player found with id no-such-player"))
    }

    @Test
    fun noSuchGame() {
        respondToInvitationSpy.outcome = {
            it.noSuchGame(GameId("no-such-game"))
        }

        makeRequest()
                .then()
                .statusCode(422)
                .body("message", equalTo("No game found with id no-such-game"))
    }

    private fun makeRequestWith(body: String): Response {
        return RestAssured
                .given()
                .contentType("application/json")
                .body(body)
                .`when`()
                .post("$baseUrl/api/v1/invitationResponses")
    }

    private fun makeRequest(): Response {
        return makeRequestWith("""
                    {
                        "guest": "some-guest-id",
                        "game": "some-game-id",
                        "accept": false
                    }
                """.trimIndent())
    }
}

class RespondToInvitationSpy : RespondToInvitation {
    var lastInvocation: Triple<PlayerId, GameId, Boolean>? = null
    lateinit var outcome: (RespondToInvitation.Outcome<*>) -> Any?

    override fun <T> invoke(guestId: PlayerId, gameId: GameId, accept: Boolean, outcome: RespondToInvitation.Outcome<T>): T {
        lastInvocation = Triple(guestId, gameId, accept)
        @Suppress("UNCHECKED_CAST")
        return this.outcome(outcome) as T
    }
}

@Configuration
@EnableAutoConfiguration
@Profile("RespondToInvitationEndpointTest")
@Import(
        RespondToInvitationSpy::class,
        RespondToInvitationEndpoint::class
)
class RespondToInvitationEndpointTestConfig