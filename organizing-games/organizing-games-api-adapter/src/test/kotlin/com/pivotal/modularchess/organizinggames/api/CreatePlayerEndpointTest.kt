package com.pivotal.modularchess.organizinggames.api

import com.pivotal.modularchess.organizinggames.models.*
import com.pivotal.modularchess.organizinggames.models.Side.BLACK
import com.pivotal.modularchess.organizinggames.ports.primary.CreatePlayer
import io.restassured.RestAssured
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles

@SpringBootTest(
        classes = [CreatePlayerEndpointTestConfig::class],
        webEnvironment = RANDOM_PORT
)
@ActiveProfiles("CreatePlayerEndpointTest")
class CreatePlayerEndpointTest {

    @LocalServerPort
    private lateinit var port: String

    private val baseUrl: String
        get() = "http://localhost:$port"

    @Autowired
    private lateinit var createGameSpy: CreatePlayerSpy

    @Test
    fun success() {
        RestAssured
                .given()
                .contentType("application/json")
                .body("""
                    {
                        "email": "the-player@example.com"
                    }
                """.trimIndent())
                .`when`()
                .post("$baseUrl/api/v1/players")
                .then()
                .statusCode(200)
                .body("id", equalTo("the-player-id"))
                .body("email", equalTo("the-player@example.com"))

        assertThat(createGameSpy.lastInvocation).isEqualTo("the-player@example.com")
    }

    @Test
    fun emailAlreadyExists() {
        createGameSpy.outcome = { it.emailAlreadyExists() }
        RestAssured
                .given()
                .contentType("application/json")
                .body("""
                    {
                        "email": "the-player@example.com"
                    }
                """.trimIndent())
                .`when`()
                .post("$baseUrl/api/v1/players")
                .then()
                .statusCode(422)
                .body("message", equalTo("That email is already taken"))
    }
}

class CreatePlayerSpy : CreatePlayer {
    var lastInvocation: String? = null
    var outcome: (CreatePlayer.Outcome<*>) -> Any? = {
        it.success(Player(
                id = PlayerId("the-player-id"),
                email = "the-player@example.com"
        ))
    }

    override fun <T> invoke(email: String, outcome: CreatePlayer.Outcome<T>): T {
        lastInvocation = email
        @Suppress("UNCHECKED_CAST")
        return this.outcome(outcome) as T
    }
}

@Configuration
@EnableAutoConfiguration
@Profile("CreatePlayerEndpointTest")
@Import(
        CreatePlayerSpy::class,
        CreatePlayerEndpoint::class
)
class CreatePlayerEndpointTestConfig