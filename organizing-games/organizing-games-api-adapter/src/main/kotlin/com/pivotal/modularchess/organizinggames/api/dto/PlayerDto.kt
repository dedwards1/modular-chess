package com.pivotal.modularchess.organizinggames.api.dto

import com.pivotal.modularchess.organizinggames.models.Player

data class PlayerDto(
        val id: String? = "",
        val email: String = ""
) {
    constructor(player: Player): this(
            id = player.id?.value,
            email = player.email
    )
}