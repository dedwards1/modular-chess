package com.pivotal.modularchess.organizinggames.api

import com.pivotal.modularchess.organizinggames.api.dto.ErrorMessageDto.Companion.error422Response
import com.pivotal.modularchess.organizinggames.api.dto.GameDto
import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.GameId
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.ports.primary.RespondToInvitation
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

data class RespondToInvitationRequest(
    val guest: String = "",
    val game: String = "",
    val accept: Boolean = true
)

class RespondToInvitationResponses: RespondToInvitation.Outcome<ResponseEntity<*>> {
    override fun success(game: Game) =
            ok(GameDto(game))

    override fun noSuchPlayer(playerId: PlayerId) =
            error422Response("No player found with id ${playerId.value}")

    override fun noSuchGame(gameId: GameId) =
            error422Response("No game found with id ${gameId.value}")

    override fun noSuchInvitation() =
            error422Response("That player does not have an invitation to that game")

    override fun cannotRespondToNonPendingInvitation(game: Game) =
            error422Response("The player's invitation to the given game is not pending")
}

@RestController
class RespondToInvitationEndpoint(
        private val respondToInvitation: RespondToInvitation
) {

    @PostMapping("/api/v1/invitationResponses")
    fun handleRespondToInvitation(@RequestBody request: RespondToInvitationRequest): ResponseEntity<*> {
        return respondToInvitation(
                guestId = PlayerId(request.guest),
                gameId = GameId(request.game),
                accept = request.accept,
                outcome = RespondToInvitationResponses()
        )
    }
}