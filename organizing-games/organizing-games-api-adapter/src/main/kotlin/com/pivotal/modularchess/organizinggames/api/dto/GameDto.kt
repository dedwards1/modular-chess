package com.pivotal.modularchess.organizinggames.api.dto

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.InvitationStatus.*
import com.pivotal.modularchess.organizinggames.models.Side.BLACK
import com.pivotal.modularchess.organizinggames.models.Side.WHITE

data class GameDto(
        val id: String,
        val host: String,
        val guest: String,
        val hostSide: String,
        val invitationStatus: String
) {
    constructor(game: Game) : this(
            id = game.id!!.value,
            host = game.host.id!!.value,
            guest = game.guest.id!!.value,
            hostSide = when (game.hostSide) {
                WHITE -> "white"
                BLACK -> "black"
            },
            invitationStatus = when (game.invitationStatus) {
                PENDING -> "pending"
                ACCEPTED -> "accepted"
                DECLINED -> "declined"
            }
    )
}