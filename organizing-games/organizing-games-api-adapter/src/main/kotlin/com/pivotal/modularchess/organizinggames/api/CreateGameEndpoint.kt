package com.pivotal.modularchess.organizinggames.api

import com.pivotal.modularchess.organizinggames.api.dto.ErrorMessageDto
import com.pivotal.modularchess.organizinggames.api.dto.GameDto
import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.InvitationStatus
import com.pivotal.modularchess.organizinggames.models.InvitationStatus.*
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.models.Side.*
import com.pivotal.modularchess.organizinggames.ports.primary.CreateGame
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.*
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

data class CreateGameRequest(
        val host: String = "",
        val guest: String = "",
        val hostSide: String = ""
)

class CreateGameResponses : CreateGame.Outcome<ResponseEntity<*>> {
    override fun success(game: Game) =
            ok().body(GameDto(game))

    override fun noSuchPlayer(id: PlayerId) =
            unprocessableEntity().body(ErrorMessageDto("No such player: ${id.value}"))
}

@RestController
class CreateGameEndpoint(
        private val createGame: CreateGame
) {

    @PostMapping("/api/v1/games")
    fun handleCreateGame(@RequestBody request: CreateGameRequest): ResponseEntity<*> {
        return createGame(
                hostId = PlayerId(request.host),
                guestId = PlayerId(request.guest),
                hostSide = when (request.hostSide) {
                    "white" -> WHITE
                    "black" -> BLACK
                    else -> throw RuntimeException("Invalid value for field 'hostSide': ${request.hostSide}")
                },
                outcome = CreateGameResponses()
        )
    }
}