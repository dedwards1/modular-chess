package com.pivotal.modularchess.organizinggames.api

import com.pivotal.modularchess.organizinggames.api.dto.ErrorMessageDto
import com.pivotal.modularchess.organizinggames.api.dto.GameDto
import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.ports.primary.GetPlayersGames
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

class GetPlayersGamesResponses : GetPlayersGames.Outcome<ResponseEntity<*>> {
    override fun success(games: List<Game>) =
            ResponseEntity.ok().body(games.map { GameDto(it) })

    override fun noSuchPlayer(playerId: PlayerId) =
            ResponseEntity.unprocessableEntity().body(ErrorMessageDto("No such player: ${playerId.value}"))
}

@RestController
class GetPlayersGamesEndpoint(
        private val getPlayersGames: GetPlayersGames
) {

    @GetMapping("/api/v1/games")
    fun handleCreateGame(@RequestParam("player") playerId: String): ResponseEntity<*> {
        return getPlayersGames(
                playerId = PlayerId(playerId),
                outcome = GetPlayersGamesResponses()
        )
    }
}