package com.pivotal.modularchess.organizinggames.api.dto

import org.springframework.http.ResponseEntity

data class ErrorMessageDto(
        val message: String
) {
    companion object {
        fun error422Response(message: String) =
            ResponseEntity.unprocessableEntity().body(ErrorMessageDto(message))
    }
}