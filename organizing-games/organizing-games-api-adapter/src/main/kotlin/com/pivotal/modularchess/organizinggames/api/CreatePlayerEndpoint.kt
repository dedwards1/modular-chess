package com.pivotal.modularchess.organizinggames.api

import com.pivotal.modularchess.organizinggames.api.dto.ErrorMessageDto
import com.pivotal.modularchess.organizinggames.api.dto.PlayerDto
import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.ports.primary.CreatePlayer
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.*
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

data class CreatePlayerRequest(
        val email: String = ""
)

class CreatePlayerResponses : CreatePlayer.Outcome<ResponseEntity<*>> {
    override fun success(player: Player) =
            ok().body(PlayerDto(player))

    override fun emailAlreadyExists() =
            unprocessableEntity().body(ErrorMessageDto("That email is already taken"))
}

@RestController
class CreatePlayerEndpoint(
        private val createPlayer: CreatePlayer
) {
    @PostMapping("/api/v1/players")
    fun handleCreatePlayer(@RequestBody request: CreatePlayerRequest): ResponseEntity<*> {
        return createPlayer(email = request.email, outcome = CreatePlayerResponses())
    }
}