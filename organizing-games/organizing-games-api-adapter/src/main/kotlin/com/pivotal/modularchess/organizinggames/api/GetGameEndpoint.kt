package com.pivotal.modularchess.organizinggames.api

import com.pivotal.modularchess.organizinggames.api.dto.GameDto
import com.pivotal.modularchess.organizinggames.models.GameId
import com.pivotal.modularchess.organizinggames.ports.secondary.GameRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class GetGameEndpoint(
        private val gameRepository: GameRepository
) {
    @GetMapping("/api/v1/games/{gameId}")
    fun handleGetGame(@PathVariable("gameId") gameId: String): GameDto {
        return GameDto(gameRepository.findGame(GameId(gameId))!!)
    }
}