package com.pivotal.modularchess.organizinggames.api

import com.pivotal.modularchess.organizinggames.api.dto.PlayerDto
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/*
 * Here's an interesting maneuver!
 * Secondary port definitions can also serve as primary ports
 * when there isn't any useful policy that needs to be handled.
 * In this case, rather than defining a GetAllPlayers primary port
 * which did nothing but delegate to the PlayerRepository,
 * I choose to have this primary adapter (the api module)
 * invoke the secondary adapter (PlayerRepository) directly.
 *
 * The dependency is still inverted, and if we ever need to
 * use "adapter extraction" to spin PlayerRepository off into
 * a separate service, it will be no different than if we had
 * a primary port defined.
 */
@RestController
class GetPlayersEndpoint(
        val playerRepository: PlayerRepository
) {
    @GetMapping("/api/v1/players")
    fun handleCreatePlayer() = playerRepository.findAllPlayers().map { PlayerDto(it) }
}