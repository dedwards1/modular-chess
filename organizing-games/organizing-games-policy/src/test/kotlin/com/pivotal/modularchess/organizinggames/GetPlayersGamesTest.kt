package com.pivotal.modularchess.organizinggames

import com.pivotal.modularchess.organizinggames.models.*
import com.pivotal.modularchess.organizinggames.models.InvitationStatus.PENDING
import com.pivotal.modularchess.organizinggames.models.Side.*
import com.pivotal.modularchess.organizinggames.ports.primary.DefaultGetPlayersGames
import com.pivotal.modularchess.organizinggames.ports.primary.GetPlayersGames
import com.pivotal.modularchess.organizinggames.ports.secondary.GameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakeGameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakePlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class GetPlayersGamesTest {

    val playerRepository = FakePlayerRepository()
    val gameRepository = FakeGameRepository()

    val getPlayersGames: GetPlayersGames = DefaultGetPlayersGames(
            playerRepository = playerRepository,
            gameRepository = gameRepository
    )

    @Test
    fun `when player exists`() {
        val judit = playerRepository.savePlayer(Player(email = "jpolgar@example.com"))
        val garry = playerRepository.savePlayer(Player(email = "gkasparov@example.com"))

        gameRepository.saveGame(Game(
                host = judit,
                guest = garry,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        getPlayersGames(playerId = judit.id!!, outcome = object : DummyGetPlayersGamesOutcome<Unit> {
            override fun success(games: List<Game>) {
                assertThat(games).isEqualTo(gameRepository.findGamesForPlayer(judit.id!!))
            }
        })
    }

    @Test
    fun `when player does not exist`() {
        val judit = playerRepository.savePlayer(Player(email = "jpolgar@example.com"))
        val garry = playerRepository.savePlayer(Player(email = "gkasparov@example.com"))

        gameRepository.saveGame(Game(
                host = judit,
                guest = garry,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        getPlayersGames(playerId = PlayerId("no such player"), outcome = object : DummyGetPlayersGamesOutcome<Unit> {
            override fun noSuchPlayer(playerId: PlayerId) {
                assertThat(playerId).isEqualTo(PlayerId("no such player"))
            }
        })
    }
}