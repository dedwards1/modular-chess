package com.pivotal.modularchess.organizinggames.fakes

import com.pivotal.modularchess.organizinggames.portcontracts.PlayerRepositoryContract
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakePlayerRepository

class FakePlayerRepositoryTest: PlayerRepositoryContract() {
    val repo = FakePlayerRepository()
    override fun repository() = repo
}