package com.pivotal.modularchess.organizinggames

import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.ports.primary.CreatePlayer
import com.pivotal.modularchess.organizinggames.ports.primary.DefaultCreatePlayer
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakePlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CreatePlayerTest {
    val playerRepo: PlayerRepository = FakePlayerRepository()
    val createPlayer: CreatePlayer = DefaultCreatePlayer(playerRepo)

    @Test
    fun `successfully creating a player`() {
        val player = createPlayer("someplayer@example.com", CreatePlayer.expectSuccess)
        assertThat(player.email).isEqualTo("someplayer@example.com")

        assertThat(playerRepo.findPlayer(player.id!!)).isEqualTo(player)
    }

    @Test
    fun `when a player with the same email already exists`() {
        createPlayer("someplayer@example.com", CreatePlayer.expectSuccess)
        createPlayer("someplayer@example.com", object : DummyCreatePlayerOutcome<Unit> {
            override fun emailAlreadyExists() {
                // pass test
            }
        })
    }
}