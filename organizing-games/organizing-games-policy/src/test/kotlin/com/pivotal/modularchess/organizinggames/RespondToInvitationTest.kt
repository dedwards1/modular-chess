package com.pivotal.modularchess.organizinggames

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.GameId
import com.pivotal.modularchess.organizinggames.models.InvitationStatus.*
import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.models.Side.WHITE
import com.pivotal.modularchess.organizinggames.ports.primary.DefaultRespondToInvitation
import com.pivotal.modularchess.organizinggames.ports.primary.RespondToInvitation
import com.pivotal.modularchess.organizinggames.ports.secondary.InitializeGame
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakeGameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakePlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class RespondToInvitationTest {

    private val playerRepo = FakePlayerRepository()
    private val gameRepo = FakeGameRepository()
    private val initializeGame = InitializeGameSpy()
    private val respondToInvitation: RespondToInvitation = DefaultRespondToInvitation(
            playerRepo = playerRepo,
            gameRepo = gameRepo,
            initializeGame = initializeGame
    )

    val judit = playerRepo.savePlayer(Player(email = "jpolgar@example.com"))
    val garry = playerRepo.savePlayer(Player(email = "gkasparov@example.com"))

    @Test
    fun `accepting a pending invitation`() {
        val game = gameRepo.saveGame(Game(
                host = judit,
                guest = garry,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        respondToInvitation(
                guestId = garry.id!!,
                gameId = game.id!!,
                accept = true,
                outcome = object : DummyRespondToInvitationOutcome<Unit> {
                    override fun success(game: Game) {
                        assertThat(game).isEqualTo(Game(
                                id = game.id,
                                host = judit,
                                guest = garry,
                                hostSide = WHITE,
                                invitationStatus = ACCEPTED
                        ))
                    }
                }
        )

        assertThat(gameRepo.findGame(game.id!!)?.invitationStatus).isEqualTo(ACCEPTED)
        assertThat(initializeGame.lastCall).isEqualTo(game.id!!)
    }

    @Test
    fun `declining a pending invitation`() {
        val game = gameRepo.saveGame(Game(
                host = judit,
                guest = garry,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        respondToInvitation(
                guestId = garry.id!!,
                gameId = game.id!!,
                accept = false,
                outcome = object : DummyRespondToInvitationOutcome<Unit> {
                    override fun success(game: Game) {
                        assertThat(game).isEqualTo(Game(
                                id = game.id,
                                host = judit,
                                guest = garry,
                                hostSide = WHITE,
                                invitationStatus = DECLINED
                        ))
                    }
                }
        )

        assertThat(gameRepo.findGame(game.id!!)?.invitationStatus).isEqualTo(DECLINED)
        assertThat(initializeGame.lastCall).isNull()
    }

    @Test
    fun `accepting an invitation with a nonsense player id`() {
        val game = gameRepo.saveGame(Game(
                host = judit,
                guest = garry,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        respondToInvitation(
                guestId = PlayerId("no such player"),
                gameId = game.id!!,
                accept = true,
                outcome = object : DummyRespondToInvitationOutcome<Unit> {
                    override fun noSuchPlayer(playerId: PlayerId) {
                        assertThat(playerId).isEqualTo(PlayerId("no such player"))
                    }
                }
        )

        assertThat(initializeGame.lastCall).isNull()
    }

    @Test
    fun `accepting an invitation with a nonsense game id`() {
        respondToInvitation(
                guestId = judit.id!!,
                gameId = GameId("no such game"),
                accept = true,
                outcome = object : DummyRespondToInvitationOutcome<Unit> {
                    override fun noSuchGame(gameId: GameId) {
                        assertThat(gameId).isEqualTo(GameId("no such game"))
                    }
                }
        )

        assertThat(initializeGame.lastCall).isNull()
    }

    @Test
    fun `trying to accept someone else's invitation`() {
        val game = gameRepo.saveGame(Game(
                host = judit,
                guest = garry,
                hostSide = WHITE,
                invitationStatus = PENDING
        ))

        respondToInvitation(
                guestId = judit.id!!, // Judit is the host, not the guest
                gameId = game.id!!,
                accept = true,
                outcome = object : DummyRespondToInvitationOutcome<Unit> {
                    override fun noSuchInvitation() {
                        // pass test
                    }
                }
        )

        assertThat(initializeGame.lastCall).isNull()
    }

    @Test
    fun `trying to respond to a non-pending invitation`() {
        val existingGame = gameRepo.saveGame(Game(
                host = judit,
                guest = garry,
                hostSide = WHITE,
                invitationStatus = DECLINED
        ))

        respondToInvitation(
                guestId = garry.id!!,
                gameId = existingGame.id!!,
                accept = true,
                outcome = object : DummyRespondToInvitationOutcome<Unit> {
                    override fun cannotRespondToNonPendingInvitation(game: Game) {
                        assertThat(game).isEqualTo(existingGame)
                    }
                }
        )

        assertThat(initializeGame.lastCall).isNull()
    }
}

class InitializeGameSpy: InitializeGame {
    var lastCall: GameId? = null

    override fun invoke(gameId: GameId) {
        lastCall = gameId
    }
}
