package com.pivotal.modularchess.organizinggames

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.GameId
import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.ports.primary.*

interface DummyCreatePlayerOutcome<T> : CreatePlayer.Outcome<T> {
    override fun success(player: Player): T =
            throw RuntimeException("success outcome should not have been invoked!")

    override fun emailAlreadyExists(): T =
            throw RuntimeException("emailAlreadyExists outcome should not have been invoked!")
}

interface DummyCreateGameOutcome<T> : CreateGame.Outcome<T> {
    override fun success(game: Game): T =
            throw RuntimeException("success outcome should not have been invoked!")

    override fun noSuchPlayer(id: PlayerId): T =
            throw RuntimeException("noSuchPlayer outcome should not have been invoked!")
}

interface DummyRespondToInvitationOutcome<T> : RespondToInvitation.Outcome<T> {
    override fun success(game: Game): T =
            throw RuntimeException("success outcome should not have been invoked!")

    override fun noSuchPlayer(playerId: PlayerId): T =
            throw RuntimeException("noSuchPlayer outcome should not have been invoked!")

    override fun noSuchGame(gameId: GameId): T =
            throw RuntimeException("noSuchGame outcome should not have been invoked!")

    override fun noSuchInvitation(): T =
            throw RuntimeException("noSuchInvitation outcome should not have been invoked!")

    override fun cannotRespondToNonPendingInvitation(game: Game): T =
            throw RuntimeException("cannotRespondToNonPendingInvitation outcome should not have been invoked!")
}

interface DummyGetPlayersGamesOutcome<T> : GetPlayersGames.Outcome<T> {
    override fun noSuchPlayer(playerId: PlayerId): T =
            throw RuntimeException("noSuchPlayer outcome should not have been invoked!")

    override fun success(games: List<Game>): T =
            throw RuntimeException("success outcome should not have been invoked!")
}