package com.pivotal.modularchess.organizinggames

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.InvitationStatus
import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.models.Side.*
import com.pivotal.modularchess.organizinggames.ports.primary.CreateGame
import com.pivotal.modularchess.organizinggames.ports.primary.DefaultCreateGame
import com.pivotal.modularchess.organizinggames.ports.secondary.GameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakeGameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakePlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CreateGameTest {

    val playerRepo = FakePlayerRepository()
    val gameRepo: GameRepository = FakeGameRepository()
    val createGame: CreateGame = DefaultCreateGame(playerRepo = playerRepo, gameRepo = gameRepo)

    val judit = playerRepo.savePlayer(Player(email = "jpolgar@example.com"))
    val garry = playerRepo.savePlayer(Player(email = "gkasparov@example.com"))

    @Test
    fun `creating a game with valid player ids`() {
        val game = createGame(
                hostId = judit.id!!,
                guestId = garry.id!!,
                hostSide = WHITE,
                outcome = CreateGame.expectSuccess
        )

        assertThat(game.host).isEqualTo(judit)
        assertThat(game.guest).isEqualTo(garry)
        assertThat(game.hostSide).isEqualTo(WHITE)
        assertThat(game.invitationStatus).isEqualTo(InvitationStatus.PENDING)

        assertThat(gameRepo.findGame(game.id!!)).isEqualTo(game)
    }

    @Test
    fun `creating a game with nonsense player ids`() {
        createGame(
                hostId = PlayerId("no such host"),
                guestId = garry.id!!,
                hostSide = WHITE,
                outcome = object : DummyCreateGameOutcome<Unit> {
                    override fun noSuchPlayer(id: PlayerId) {
                        assertThat(id).isEqualTo(PlayerId("no such host"))
                    }
                }
        )

        createGame(
                hostId = judit.id!!,
                guestId = PlayerId("no such guest"),
                hostSide = WHITE,
                outcome = object : DummyCreateGameOutcome<Unit> {
                    override fun noSuchPlayer(id: PlayerId) {
                        assertThat(id).isEqualTo(PlayerId("no such guest"))
                    }
                }
        )
    }
}