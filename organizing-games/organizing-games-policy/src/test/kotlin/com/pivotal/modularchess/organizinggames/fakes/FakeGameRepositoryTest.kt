package com.pivotal.modularchess.organizinggames.fakes

import com.pivotal.modularchess.organizinggames.portcontracts.GameRepositoryContract
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakeGameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.fakes.FakePlayerRepository

class FakeGameRepositoryTest: GameRepositoryContract() {
    private val playerRepo = FakePlayerRepository()
    private val gameRepo = FakeGameRepository()

    override fun playerRepository() = playerRepo
    override fun gameRepository() = gameRepo
}