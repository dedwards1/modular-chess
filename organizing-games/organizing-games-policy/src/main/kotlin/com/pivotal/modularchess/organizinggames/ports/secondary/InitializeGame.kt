package com.pivotal.modularchess.organizinggames.ports.secondary

import com.pivotal.modularchess.organizinggames.models.GameId

interface InitializeGame {
    operator fun invoke(gameId: GameId)
}
