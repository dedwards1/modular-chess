package com.pivotal.modularchess.organizinggames.models

enum class Side {
    WHITE,
    BLACK
}
