package com.pivotal.modularchess.organizinggames.ports.secondary

import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.models.PlayerId

interface PlayerRepository {
    fun findPlayer(id: PlayerId): Player?
    fun savePlayer(player: Player): Player
    fun findPlayerByEmail(email: String): Player?
    fun findAllPlayers(): List<Player>
}
