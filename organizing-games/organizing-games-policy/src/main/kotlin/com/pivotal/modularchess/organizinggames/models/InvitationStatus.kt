package com.pivotal.modularchess.organizinggames.models

enum class InvitationStatus {
    PENDING,
    ACCEPTED,
    DECLINED
}
