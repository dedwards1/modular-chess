package com.pivotal.modularchess.organizinggames.ports.primary

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.GameId
import com.pivotal.modularchess.organizinggames.models.InvitationStatus.*
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.ports.secondary.GameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.InitializeGame
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository

interface RespondToInvitation {
    operator fun <T> invoke(
            guestId: PlayerId,
            gameId: GameId,
            accept: Boolean,
            outcome: Outcome<T>
    ): T

    interface Outcome<T> {
        fun success(game: Game): T
        fun noSuchPlayer(playerId: PlayerId): T
        fun noSuchGame(gameId: GameId): T
        fun noSuchInvitation(): T
        fun cannotRespondToNonPendingInvitation(game: Game): T
    }
}

class DefaultRespondToInvitation(
        private val playerRepo: PlayerRepository,
        private val gameRepo: GameRepository,
        private val initializeGame: InitializeGame
) : RespondToInvitation {
    override fun <T> invoke(
            guestId: PlayerId,
            gameId: GameId,
            accept: Boolean,
            outcome: RespondToInvitation.Outcome<T>
    ): T {
        if (playerRepo.findPlayer(guestId) == null) return outcome.noSuchPlayer(guestId)

        val game = gameRepo.findGame(gameId) ?: return outcome.noSuchGame(gameId)
        if (game.guest.id != guestId) return outcome.noSuchInvitation()
        if (game.invitationStatus != PENDING) return outcome.cannotRespondToNonPendingInvitation(game)

        val updatedGame = gameRepo.saveGame(gameRepo.findGame(gameId)!!.copy(
                invitationStatus = if (accept) ACCEPTED else DECLINED
        ))
        if (accept) initializeGame(updatedGame.id!!)
        return outcome.success(updatedGame)
    }
}