package com.pivotal.modularchess.organizinggames.ports.primary

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.ports.secondary.GameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository

interface GetPlayersGames {
    operator fun <T> invoke(playerId: PlayerId, outcome: Outcome<T>): T

    interface Outcome<T> {
        fun success(games: List<Game>): T
        fun noSuchPlayer(playerId: PlayerId): T
    }
}

class DefaultGetPlayersGames(
        private val playerRepository: PlayerRepository,
        private val gameRepository: GameRepository
) : GetPlayersGames {
    override fun <T> invoke(playerId: PlayerId, outcome: GetPlayersGames.Outcome<T>): T {
        if(playerRepository.findPlayer(playerId) == null) return outcome.noSuchPlayer(playerId)

        return outcome.success(gameRepository.findGamesForPlayer(playerId))
    }
}