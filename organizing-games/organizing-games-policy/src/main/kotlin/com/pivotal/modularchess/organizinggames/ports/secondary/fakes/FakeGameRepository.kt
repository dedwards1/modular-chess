package com.pivotal.modularchess.organizinggames.ports.secondary.fakes

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.GameId
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.ports.secondary.GameRepository
import java.util.*

class FakeGameRepository : GameRepository {
    private val games = mutableMapOf<GameId, Game>()

    override fun saveGame(game: Game): Game {
        val savedGame = if(game.id != null)
            game
        else
            game.copy(id = GameId(UUID.randomUUID().toString()))

        games[savedGame.id!!] = savedGame
        return savedGame
    }

    override fun findGame(id: GameId): Game? {
        return games[id]
    }

    override fun findGamesForPlayer(id: PlayerId): List<Game> {
        return games.values.filter {
            it.host.id == id || it.guest.id == id
        }
    }
}
