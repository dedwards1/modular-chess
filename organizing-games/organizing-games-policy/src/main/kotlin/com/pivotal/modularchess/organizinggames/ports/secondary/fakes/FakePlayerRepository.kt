package com.pivotal.modularchess.organizinggames.ports.secondary.fakes

import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.models.PlayerId
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository
import java.util.*

class FakePlayerRepository : PlayerRepository {
    private val players = mutableMapOf<PlayerId, Player>()

    override fun findPlayer(id: PlayerId): Player? {
        return players[id]
    }

    override fun savePlayer(player: Player): Player {
        val savedPlayer =  player.copy(id = PlayerId(UUID.randomUUID().toString()))
        players[savedPlayer.id!!] = savedPlayer
        return savedPlayer
    }

    override fun findPlayerByEmail(email: String): Player? {
        return players.values.firstOrNull { it.email == email }
    }

    override fun findAllPlayers(): List<Player> {
        return players.values.toList()
    }
}
