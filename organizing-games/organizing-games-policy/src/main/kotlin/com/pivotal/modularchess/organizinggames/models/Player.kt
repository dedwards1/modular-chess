package com.pivotal.modularchess.organizinggames.models

data class Player(
    val id: PlayerId? = null,
    val email: String
)

data class PlayerId(val value: String)