package com.pivotal.modularchess.organizinggames.ports.primary

import com.pivotal.modularchess.organizinggames.models.*
import com.pivotal.modularchess.organizinggames.models.InvitationStatus.ACCEPTED
import com.pivotal.modularchess.organizinggames.models.InvitationStatus.PENDING
import com.pivotal.modularchess.organizinggames.ports.secondary.GameRepository
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository

interface CreateGame {
    operator fun <T> invoke(
            hostId: PlayerId,
            guestId: PlayerId,
            hostSide: Side,
            outcome: Outcome<T>
    ): T

    interface Outcome<T> {
        fun success(game: Game): T
        fun noSuchPlayer(id: PlayerId): T
    }

    companion object {
        val expectSuccess = object : Outcome<Game> {
            override fun success(game: Game) = game
            override fun noSuchPlayer(id: PlayerId)
                = throw RuntimeException("Expected success outcome, but noSuchPlayer was invoked!")
        }
    }
}

class DefaultCreateGame(
        private val playerRepo: PlayerRepository,
        private val gameRepo: GameRepository
) : CreateGame {
    override fun <T> invoke(
            hostId: PlayerId,
            guestId: PlayerId,
            hostSide: Side,
            outcome: CreateGame.Outcome<T>
    ): T {
        val host = playerRepo.findPlayer(hostId) ?: return outcome.noSuchPlayer(hostId)
        val guest = playerRepo.findPlayer(guestId) ?: return outcome.noSuchPlayer(guestId)

        return outcome.success(gameRepo.saveGame(Game(
                host = host,
                guest = guest,
                hostSide = hostSide,
                invitationStatus = PENDING
        )))
    }
}