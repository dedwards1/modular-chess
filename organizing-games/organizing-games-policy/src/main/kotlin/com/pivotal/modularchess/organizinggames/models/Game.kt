package com.pivotal.modularchess.organizinggames.models

data class Game(
        val id: GameId? = null,
        val host: Player,
        val guest: Player,
        val hostSide: Side,
        val invitationStatus: InvitationStatus
)

data class GameId(val value: String)