package com.pivotal.modularchess.organizinggames.ports.secondary

import com.pivotal.modularchess.organizinggames.models.Game
import com.pivotal.modularchess.organizinggames.models.GameId
import com.pivotal.modularchess.organizinggames.models.PlayerId

interface GameRepository {
    fun saveGame(game: Game): Game
    fun findGame(id: GameId): Game?
    fun findGamesForPlayer(id: PlayerId): List<Game>
}