package com.pivotal.modularchess.organizinggames.ports.primary

import com.pivotal.modularchess.organizinggames.models.Player
import com.pivotal.modularchess.organizinggames.ports.secondary.PlayerRepository

interface CreatePlayer {
    operator fun <T> invoke(email: String, outcome: Outcome<T>): T

    interface Outcome<T> {
        fun success(player: Player): T
        fun emailAlreadyExists(): T
    }

    companion object {
        val expectSuccess = object : Outcome<Player> {
            override fun success(player: Player) = player
            override fun emailAlreadyExists() = throw RuntimeException("Expected success outcome, not emailAlreadyExists")
        }
    }
}

class DefaultCreatePlayer(
        private val playerRepo: PlayerRepository
) : CreatePlayer {
    override operator fun <T> invoke(email: String, outcome: CreatePlayer.Outcome<T>): T {
        if(playerRepo.findPlayerByEmail(email) != null) return outcome.emailAlreadyExists()
        return outcome.success(playerRepo.savePlayer(Player(email = email)))
    }
}
