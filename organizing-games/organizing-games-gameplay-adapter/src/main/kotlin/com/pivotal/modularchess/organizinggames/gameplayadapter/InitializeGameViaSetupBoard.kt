package com.pivotal.modularchess.organizinggames.gameplayadapter

import com.pivotal.modularchess.gameplay.ports.primary.SetupBoard
import com.pivotal.modularchess.organizinggames.ports.secondary.InitializeGame

class InitializeGameViaSetupBoard(
        val setupBoard: SetupBoard
): InitializeGame {
    override fun invoke(gameId: OrganizingGamesGameId) {
        setupBoard(GameplayGameId(gameId.value))
    }
}

/* Kotlin typealiases are super useful here.
 * Both organizing-games-policy and gameplay have a definition of "GameId";
 * in any other module, this wouldn't be ambiguous, but in this adapter,
 * since we have to speak both languages, it's ambiguous which one "GameId" refers to.
 * In Java you would have to use the fully-qualified name everywhere,
 * but in Kotlin, we can define a local typealias to tell the types apart. */
typealias OrganizingGamesGameId = com.pivotal.modularchess.organizinggames.models.GameId
typealias GameplayGameId = com.pivotal.modularchess.gameplay.models.GameId