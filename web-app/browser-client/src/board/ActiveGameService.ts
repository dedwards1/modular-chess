import { useState, useContext, useEffect, createContext } from "react"

export const activeGameService = (
  boardStateUrl: string,
  getGameUrl: string,
) => {
  const objId = `active-game-service-${Math.round(Math.random()*1e4)}`

  const subscriptions = {}
  let pending = false
  let activeGame: string | null = null

  function refresh() {
    if(!pending && activeGame) {
      pending = true
      Promise.all([
        fetch(`${boardStateUrl}/${activeGame}`).then(response => response.json()),
        fetch(`${getGameUrl}/${activeGame}`).then(response => response.json()),
      ]).then(([boardState, game]) => {
        for(let id in subscriptions) subscriptions[id]({ game, boardState })
        pending = false
      })
    }
  }

  function subscribe(callback) {
    const id = Math.round(Math.random() * 1e8)
    subscriptions[id] = callback
    refresh()
    return id
  }

  function unsubscribe(id) {
    delete subscriptions[id]
  }

  function selectGame(gameId: string) {
    activeGame = gameId
    refresh()
  }

  return {
    subscribe,
    unsubscribe,
    refresh,
    selectGame,
    id: objId
  }
}

export const ActiveGameServiceContext = createContext(activeGameService(
  `${process.env.BASE_URL}/webapp/boardState`,
  `${process.env.BASE_URL}/webapp/games`,
))

export const useActiveGame = (caller) => {
  const [activeGame, updateActiveGame] = useState({})
  const [activeGameServiceSubscription, updateActiveGameServiceSubscription] = useState(null)
  const activeGameService = useContext(ActiveGameServiceContext)

  useEffect(() => {
    updateActiveGameServiceSubscription(activeGameService.subscribe(updateActiveGame))
    return () => { activeGameService.unsubscribe(activeGameServiceSubscription) }
  }, [])

  return [activeGame, activeGameService.refresh, activeGameService.selectGame]
}
