import * as React from "react"
import {useActiveGame} from "./ActiveGameService";
import {useEffect, useState} from "react";

const pieceGlyphs = {
  white: {
    R: "♖",
    N: "♘",
    B: "♗",
    Q: "♕",
    K: "♔",
    "": "♙",
  },
  black: {
    R: "♜",
    N: "♞",
    B: "♝",
    Q: "♛",
    K: "♚",
    "": "♟",
  }
}

const Space = ({piece, row, column, selectedSpace, onSelect}: {
  piece: { side: string, type: string } | null,
  row: number,
  column: number,
  selectedSpace: string,
  onSelect: (spaceName: string) => void
}) => {
  const [highlighted, updateHighlighted] = useState(false)
  const spaceName = `${'abcdefgh'[column]}${row+1}`
  const selected = selectedSpace === spaceName
  const dark = row % 2 !== column % 2

  return <div
    className={`space ${dark ? "dark" : "light"} ${highlighted ? "highlighted" : ""} ${selected ? "selected" : ""}`}
    onMouseOver={() => updateHighlighted(true)}
    onMouseOut={() => updateHighlighted(false)}
    onClick={() => onSelect(spaceName)}
  >
    {piece && pieceGlyphs[piece.side][piece.type]}
  </div>
}

export const BoardPanel = ({signedInAs}) => {
  const [{game, boardState}, refreshBoard]:
    [{ gameId: string, boardState: { whoseTurn: string, rows: any[] } }, () => void]
    = useActiveGame("board-panel")
  const [metronome, updateMetronome] = useState(undefined)
  const [selectedSpace, updateSelectedSpace] = useState(null)

  useEffect(() => {
    const id = setInterval(() => {
      refreshBoard()
    }, 1000)
    updateMetronome(id)

    return () => {
      clearInterval(metronome as number)
    }
  }, [])

  if (!game) return <div className="panel board-panel"/>

  const playingBlack = (game.hostSide === "black" && signedInAs === game.host)
    || (game.hostSide === "white" && signedInAs === game.guest)

  const playerView = rows => playingBlack
    ? rows.slice().map(row => row.slice().reverse())
    : rows.slice().reverse()

  const playersTurn = (boardState.whoseTurn === "black" && playingBlack)
    || (boardState.whoseTurn === "white" && !playingBlack)

  const makeMove = async ({from, to}) => {
    await fetch(`${process.env.BASE_URL}/webapp/moves`, {
      method: "POST",
      headers: {"Accept": "application/json", "Content-Type": "application/json"},
      body: JSON.stringify({
        gameId: game.id,
        from,
        to
      })
    })
    refreshBoard()
  }

  const onSelect = async (newSelectedSpace: string) => {
    if(!playersTurn) return

    if(!selectedSpace) {
      updateSelectedSpace(newSelectedSpace)
      return
    }

    if(selectedSpace && selectedSpace === newSelectedSpace) {
      updateSelectedSpace(null)
      return
    }

    await makeMove({from: selectedSpace, to: newSelectedSpace})
    updateSelectedSpace(null)
  }

  let board = boardState.rows.map((row, rowIndex) =>
    row.map((piece, columnIndex) =>
      <Space
        key={`space-${rowIndex}-${columnIndex}`}
        piece={piece}
        row={rowIndex}
        column={columnIndex}
        selectedSpace={selectedSpace}
        onSelect={onSelect}
      />
    )
  );

  return <div className="panel board-panel">
    <div className="board">
      <div>{playerView(board).map((row, rowIndex) => <div key={`row-${rowIndex}`}>{row}</div>)}</div>
      <p style={{width: "100%", textAlign: "center"}}>{playersTurn ? "Your" : "Opponent's"} turn</p>
    </div>
  </div>;
}