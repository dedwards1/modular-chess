import * as React from "react"
import {useEffect, useState} from "react";

import {usePlayersList} from "../players/PlayerService"
import {useActiveGame} from "../board/ActiveGameService";

const Game = ({game, playersList, playerId, refreshGames}: {
  game: { id: string, host: string, guest: string, invitationStatus: string },
  playersList: any[],
  playerId: string,
  refreshGames: () => void
}) => {
  const [, , selectGame] = useActiveGame(`game-${game.id}`)

  const playerRole = game.host === playerId
    ? "host"
    : "guest"

  const opponentId = playerRole === "host"
    ? game.guest
    : game.host

  const opponent = playersList.filter(player => player.id === opponentId)[0]

  const play = () => selectGame(game.id)

  const respondToInvitation = async (accept: boolean) => {
    await fetch(`${process.env.BASE_URL}/webapp/invitations`, {
      method: "POST",
      headers: {"Accept": "application/json", "Content-Type": "application/json"},
      body: JSON.stringify({
        game: game.id,
        guest: playerId,
        accept: accept
      })
    })

    refreshGames()
  }

  const accept = () => respondToInvitation(true)

  const decline = () => respondToInvitation(false)

  const actions = {
    host: {
      pending: <span className="pending-action">PENDING</span>,
      accepted: <button className="play-action" onClick={play}>PLAY</button>,
      declined: <span className="declined-action">DECLINED</span>,
    },

    guest: {
      pending: <>
        <button className="accept-action" onClick={accept}>ACCEPT</button>
        <button className="decline-action" onClick={decline}>DECLINE</button>
      </>,
      accepted: <button className="play-action" onClick={play}>PLAY</button>,
      declined: <span className="declined-action">DECLINED</span>,
    }
  }

  return opponent
    ? <li>Game vs. {opponent.email} {actions[playerRole][game.invitationStatus]}</li>
    : <li>Game {actions[playerRole][game.invitationStatus]}</li>
}

export const GamesPanel = ({signedInAs}) => {
  const [gamesList, updateGamesList] = useState([])

  const [playersList] = usePlayersList()

  async function fetchGames() {
    const gamesResponse = await fetch(`${process.env.BASE_URL}/webapp/players/${signedInAs}/games`)
    updateGamesList(await gamesResponse.json())
  }

  async function createGame(e: Event) {
    e.preventDefault()
    if (signedInAs) {
      await fetch(`${process.env.BASE_URL}/webapp/games`, {
        method: "POST",
        headers: {"Accept": "application/json", "Content-Type": "application/json"},
        body: JSON.stringify({
          host: signedInAs,
          guest: opponent,
          hostSide: side
        })
      })

      fetchGames()
    }
  }

  useEffect(() => {
    if (signedInAs) {
      fetchGames()
    }
  }, [signedInAs])

  const possibleOpponents = playersList.filter(player => player.id !== signedInAs)
  const [opponent, updateOpponent] = useState("")
  const [side, updateSide] = useState("white")

  return <div className="panel games-panel">
    <h2>Games</h2>
    <ul className="games-list">{
      gamesList.map(game =>
        <Game
          key={`game-${game.id}`}
          game={game}
          playerId={signedInAs}
          playersList={playersList}
          refreshGames={fetchGames}
        />
      )
    }</ul>
    {signedInAs &&
    <form onSubmit={createGame}>
      <h3>Create Game</h3>
      <p>
        <label>Opponent: <select value={opponent} onChange={e => updateOpponent(e.target.value)}>
          <option value="">(Choose opponent)</option>
          {possibleOpponents.map(player => <option key={`opponent-${player.id}`}
                                                   value={player.id}>{player.email}</option>)}
        </select></label>
        <label>Choose side: <select value={side} onChange={e => updateSide(e.target.value)}>
          <option value="white">White</option>
          <option value="black">Black</option>
        </select></label>
      </p>
      <p>
        <button>Submit</button>
      </p>
    </form>
    }
  </div>;
}

