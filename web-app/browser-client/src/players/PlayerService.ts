import { useState, useContext, useEffect, createContext } from "react"

export const playerService = playersUrl => {
  const subscriptions = {}
  let pending = false

  async function refresh() {
    if(!pending) {
      pending = true
      const playersResponse = await fetch(playersUrl)
      const playersList = await playersResponse.json()
      for(let id in subscriptions) subscriptions[id](playersList)
      pending = false
    }
  }

  function subscribe(callback) {
    const id = Math.round(Math.random() * 1e8)
    subscriptions[id] = callback
    refresh()
    return id
  }

  function unsubscribe(id) {
    delete subscriptions[id]
  }

  return {
    subscribe,
    unsubscribe,
    refresh,
  }
}

export const PlayerServiceContext = createContext(playerService(`${process.env.BASE_URL}/webapp/players`))

export const usePlayersList = () => {
  const [playersList, updatePlayersList] = useState([])
  const [playerServiceSubscription, updatePlayerServiceSubscription] = useState(null)
  const playerService = useContext(PlayerServiceContext)

  useEffect(() => {
    updatePlayerServiceSubscription(playerService.subscribe(updatePlayersList))
    return () => { playerService.unsubscribe(playerServiceSubscription) }
  }, [])

  return [playersList, playerService.refresh]
}
