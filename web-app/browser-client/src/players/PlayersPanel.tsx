import * as React from "react"
import {useEffect, useState} from "react";
import {usePlayersList} from "./PlayerService";

const Player = ({
                  player,
                  signedInAs,
                  signInAs,
                }) => {
  const currentPlayer = player.id === signedInAs
  const signInButton = <button onClick={() => signInAs(player.id)}>Sign In</button>

  return <li>{player.email} {!currentPlayer && signInButton}</li>
}

export const PlayersPanel = props => {
  const [emailField, updateEmailField] = useState("")
  const [playersList, refreshPlayersList] = usePlayersList()

  async function createPlayer(e: Event) {
    e.preventDefault()

    await fetch(`${process.env.BASE_URL}/webapp/players`, {
      method: "POST",
      headers: {"Accept": "application/json", "Content-Type": "application/json"},
      body: JSON.stringify({email: emailField})
    })

    refreshPlayersList()
  }

  return <div className="panel players-panel">
    <h2>Players</h2>
    <ul className="players-list">
      {playersList.map(player => <Player key={`sdlkj${player.id}`} player={player} {...props}/>)}
    </ul>
    <form onSubmit={createPlayer}>
      <h3>Create Player</h3>
      <label>Email: <input value={emailField} onChange={e => updateEmailField(e.target.value)}/></label>
      <button>Submit</button>
    </form>
  </div>;
}