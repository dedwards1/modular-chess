import * as React from "react"
import {useState} from "react"

import "./styles/main.css"
import {PlayerServiceContext, playerService} from "./players/PlayerService"
import {ActiveGameServiceContext, activeGameService} from "./board/ActiveGameService";
import {PlayersPanel} from "./players/PlayersPanel"
import {GamesPanel} from "./games/GamesPanel"
import {BoardPanel} from "./board/BoardPanel"

const playerServiceSingleton = playerService(`${process.env.BASE_URL}/webapp/players`);
const activeGameServiceSingleton = activeGameService(
  `${process.env.BASE_URL}/webapp/boardState`,
  `${process.env.BASE_URL}/webapp/games`,
);

export const App = props => {
  const [signedInAs, signInAs] = useState(null)

  return <main>
    <PlayerServiceContext.Provider value={playerServiceSingleton}>
      <ActiveGameServiceContext.Provider value={activeGameServiceSingleton}>
        <PlayersPanel signedInAs={signedInAs} signInAs={signInAs}/>
        <GamesPanel signedInAs={signedInAs}/>
        <BoardPanel signedInAs={signedInAs}/>
      </ActiveGameServiceContext.Provider>
    </PlayerServiceContext.Provider>
  </main>
}
