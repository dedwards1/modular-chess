import com.moowork.gradle.node.yarn.YarnTask

plugins {
    id("com.github.node-gradle.node") version "2.2.3"
}

tasks.register<YarnTask>("install") {
    inputs.files(file("package.json"))
    outputs.files(fileTree("node_modules"), file("yarn.lock"))

    setArgs(listOf("install"))
}

tasks.register<YarnTask>("build") {
    dependsOn("install")
    inputs.files(fileTree("src"), file("package.json"), file(".env.production"))
    outputs.dir("dist")

    setArgs(listOf("build"))
}

tasks.register<Delete>("clean") {
    delete("dist", ".cache")
}

tasks.register<Delete>("devServer") {
    dependsOn("install", "yarn_start")
}