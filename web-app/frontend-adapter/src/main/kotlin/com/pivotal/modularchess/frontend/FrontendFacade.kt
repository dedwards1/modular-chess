package com.pivotal.modularchess.frontend

import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange
import java.net.URI

@Suppress("SpringJavaInjectionPointsAutowiringInspection")
@RestController
@CrossOrigin(origins = [ "http://localhost:1234" ])
class FrontendFacade(
        private val restTemplate: RestTemplate,
        private val createGameUrl: String,
        private val createPlayerUrl: String,
        private val respondToInvitationUrl: String,
        private val getBoardStateUrl: String,
        private val makeMoveUrl: String,
        private val getPlayersUrl: String,
        private val getGamesForPlayerUrl: String,
        val getGameUrl: String
) {
    @PostMapping("/webapp/games")
    fun createGame(requestEntity: RequestEntity<*>) = forward(requestEntity, createGameUrl)

    @GetMapping("/webapp/players/{playerId}/games")
    fun getGamesForPlayer(
            @PathVariable("playerId") playerId: String,
            requestEntity: RequestEntity<*>
    ) = forward(requestEntity, "$getGamesForPlayerUrl?player=$playerId")

    @PostMapping("/webapp/players")
    fun createPlayer(requestEntity: RequestEntity<*>) = forward(requestEntity, createPlayerUrl)

    @GetMapping("/webapp/players")
    fun getPlayers(requestEntity: RequestEntity<*>) = forward(requestEntity, getPlayersUrl)

    @PostMapping("/webapp/invitations")
    fun respondToInvitation(requestEntity: RequestEntity<*>) = forward(requestEntity, respondToInvitationUrl)

    @GetMapping("/webapp/games/{gameId}")
    fun getGame(requestEntity: RequestEntity<*>, @PathVariable("gameId") gameId: String)
            = forward(requestEntity, "$getGameUrl/$gameId")

    @GetMapping("/webapp/boardState/{gameId}")
    fun getBoardState(requestEntity: RequestEntity<*>, @PathVariable("gameId") gameId: String)
            = forward(requestEntity, "$getBoardStateUrl/$gameId")

    @PostMapping("/webapp/moves")
    fun makeMove(requestEntity: RequestEntity<*>) = forward(requestEntity, makeMoveUrl)

    private fun forward(requestEntity: RequestEntity<*>, url: String): ResponseEntity<Any> {
        val response = restTemplate.exchange<Any>(RequestEntity(
                requestEntity.body,
                requestEntity.headers,
                requestEntity.method,
                URI.create(url)
        ))

        return ResponseEntity(
                response.body,
                response.statusCode
        )
    }
}
